package KvetkaSpectrumLib.Models;

import java.util.HashMap;
import java.util.Map;

public class SignalOnTrackingModel {
    public double Frequency = 0.0;
    public double Range = 0.0;

    public SignalOnTrackingModel(double frequency, double range)
    {
        Frequency = frequency;
        Range = range;
    }

    public Map toMap()
    {
        HashMap<String, String> serviceAsMap = new HashMap<>();
        serviceAsMap.put("frequency", String.valueOf(Frequency));
        serviceAsMap.put("range", String.valueOf(Range));
        serviceAsMap.put("className", this.getClass().getName() + ".class");
        return serviceAsMap;
    }
}
