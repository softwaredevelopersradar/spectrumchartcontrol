package KvetkaSpectrumLib.OtherControls;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Filters extends ComboBoxCustomControl{

    public Filters() throws IOException {
        super();
        setLabelText("Фильтр");
        setRectangleWidth(149.0);
    }

    @Override
    public void setDefaultItem()
    {
        List<String> list = new ArrayList<>();
        for(int i = 0; i < 54; i+=4)
            list.add(Integer.toString(i));
        list.add("60");

        ObservableList<String> langs = FXCollections.observableArrayList(list);

        comboBox.setItems(langs);
        comboBox.setValue(list.get(0));
    }
}
