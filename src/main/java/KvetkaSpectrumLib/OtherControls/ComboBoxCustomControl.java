package KvetkaSpectrumLib.OtherControls;

import com.sun.javafx.scene.layout.region.Margins;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ComboBoxCustomControl extends AnchorPane {

    private final RadialGradient gradientPaint_Green = new RadialGradient(360,30,0,0,10,false, CycleMethod.NO_CYCLE,
            new Stop(0, Color.rgb(160,255,130)), new Stop(1, Color.rgb(30,98,10)));
    private final RadialGradient gradientPaint_Red = new RadialGradient(360,20,0,0,10,false, CycleMethod.NO_CYCLE,
            new Stop(0, Color.rgb(255,160,160)), new Stop(1, Color.rgb(144,2,2)));

    @FXML
    protected ComboBox comboBox;
    @FXML
    protected Label label;
    @FXML
    protected Circle answerAttenuatorSignalMarker;
    @FXML
    protected Rectangle rectangle;

    public ComboBoxCustomControl() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Attenuators.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        setZeroAsDefault();
    }

    public void setLabelText(String text)
    {
        label.setText(text);
    }
    public void setRectangleWidth(double width)
    {
        rectangle.setWidth(width);
    }

    public ReadOnlyObjectProperty getSelectionModelListener()
    {
        return comboBox.getSelectionModel().selectedItemProperty();
    }

    public String getSelectedItem()
    {
        String str = String.valueOf(comboBox.getValue());
        if (!str.equals("null"))
            return str;
        else
        {
            if(comboBox.getItems().size() == 0)
                return "-1";
            else  return String.valueOf(comboBox.getItems().get(0));
        }
    }

    public <T> void AddItemsList(List<T> list)
    {
        if(list != null){
            comboBox.getItems().clear();
            List<String> newList = new ArrayList<>();


            if(!list.contains(0.0) && !list.contains(0))
            {
                if(list.get(0).getClass() == Double.class || list.get(0).getClass() == Float.class)
                    newList.add("0.0");
                else newList.add("0");
            }

            for(T attenuator : list)
                newList.add(attenuator.toString());
            ObservableList<String> langs = FXCollections.observableArrayList(newList);

            comboBox.setItems(langs);
            comboBox.setValue(newList.get(0));
        }
    }

    public void AddItemsListString(List<String> list)
    {
        if(list != null){
            comboBox.getItems().clear();

            if(!list.contains("0"))
                list.add(0,"0");

            ObservableList<String> langs = FXCollections.observableArrayList(list);

            comboBox.setItems(langs);
            comboBox.setValue(list.get(0));
        }
    }

    public void AddOneItem(String attenuator)
    {
        if(attenuator != null){
            comboBox.getItems().add(attenuator);
        }
    }

    public void AddOneItem(int index, String attenuator)
    {
        if(attenuator != null){
            comboBox.getItems().add(index, attenuator);
        }
    }

    public void SetOneItem(int index, String attenuator)
    {
        if(attenuator != null){
            comboBox.getItems().set(index, attenuator);
        }
    }

    public void setItemInSignalState(boolean isLaunched)
    {
        if(isLaunched)
            answerAttenuatorSignalMarker.setFill(gradientPaint_Green);
        else
            answerAttenuatorSignalMarker.setFill(gradientPaint_Red);
    }

    public void setDefaultItem()
    {
        List<String> list = new ArrayList<>();
        for(int i = 0; i < 54; i+=4)
            list.add(Integer.toString(i));
        list.add("60");

        ObservableList<String> langs = FXCollections.observableArrayList(list);

        comboBox.setItems(langs);
        comboBox.setValue(list.get(0));
    }

    private void setZeroAsDefault()
    {
        List<String> list = new ArrayList<>();
        list.add("0");

        ObservableList<String> langs = FXCollections.observableArrayList(list);

        comboBox.setItems(langs);
        comboBox.setValue(list.get(0));
    }
}
