package KvetkaSpectrumLib.Waterfall;

import KvetkaSpectrumLib.CustomRenders.ContourDataSetRendererCustom;
import KvetkaSpectrumLib.CustomRenders.Point;
import KvetkaSpectrumLib.CustomStringConverter.CustomTickLabelFormatter;
import KvetkaSpectrumLib.CustomZoom.CustomZoomer;
import com.sun.prism.impl.PrismSettings;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.Axis;
import de.gsi.chart.axes.AxisMode;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.plugins.EditAxis;
import de.gsi.chart.plugins.Zoomer;
import de.gsi.chart.renderer.ContourType;
import de.gsi.chart.renderer.datareduction.ReductionType;
import de.gsi.chart.renderer.spi.ContourDataSetRenderer;
import de.gsi.chart.renderer.spi.utils.ColorGradient;
import de.gsi.chart.ui.geometry.Side;
import javafx.geometry.Insets;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;

import javax.swing.plaf.IconUIResource;


public class WaterfallPerformance extends XYChart {

    private TestDataSetSource dataSet;
    private DefaultNumericAxis zAxis;

    public WaterfallPerformance()
    {
        super(new DefaultNumericAxis(),new DefaultNumericAxis());
        dataSet = new TestDataSetSource();
        InitializeComponents();
    }

    public void DataSetInit(int size)
    {
        dataSet.Init(size);
    }

    private void InitializeComponents() {
        this.setAnimated(false);
        this.getRenderers().clear();
        this.setLegendVisible(false);

        this.getPlotArea().setBorder(new Border(new BorderStroke(Color.WHITE,
                BorderStrokeStyle.SOLID,
                new CornerRadii(0d), new BorderWidths(0,0,0,1))));

        this.getXAxis().setAutoRanging(false);
        this.getXAxis().setAnimated(false);
        this.getXAxis().setName("X Position");
        this.getXAxis().set(1500, 30000);
        ((DefaultNumericAxis)this.getXAxis()).setTickLabelFormatter(new CustomTickLabelFormatter());

        this.getYAxis().setAnimated(false);
        this.getYAxis().setName("Y Position");
        this.getYAxis().setAutoRanging(true);
        this.getYAxis().setSide(Side.LEFT);
        this.getYAxis().set(0,0);
        this.getYAxis().getCanvas().setWidth(0);
        this.getYAxis().getCanvas().setVisible(false);

        this.getAxesAndCanvasPane().setPadding(new Insets(0,0,0,-70));

        zAxis = new DefaultNumericAxis();
        this.getAxes().add(2,zAxis);
        zAxis.setVisible(true);
        zAxis.setAnimated(false);
        zAxis.setAutoRangeRounding(false);
        zAxis.setName("Freq");
        zAxis.setAutoRanging(false);
        zAxis.setSide(Side.LEFT);
        zAxis.set(-120,30);
        zAxis.setTranslateX(-15);
//        zAxis.setStyle(".axis {\n" +
//                "-fx-tick-label-font-size: 0.9em;\n" +
//                "-fx-tick-label-fill: #000000;\n" +
//                "}\n" +
//                ".axis-tick-mark {\n" +
//                "    -fx-stroke: black;\n" +
//                "}\n" +
//                ".axis-minor-tick-mark {\n" +
//                "    -fx-stroke: black;\n" +
//                "}");
        zAxis.setMaxWidth(35);



        final ContourDataSetRendererCustom contourRenderer = new ContourDataSetRendererCustom();
        contourRenderer.getAxes().addAll(this.getXAxis(), this.getYAxis(), zAxis);
        contourRenderer.setContourType(ContourType.HEATMAP);
        contourRenderer.setColorGradient(ColorGradient.RAINBOW_EQ);
        contourRenderer.setColorGradient(new ColorGradient("CUSTOM",
                new Stop(0.0, Color.TRANSPARENT),
                new Stop(0.255, Color.BLUE),
                new Stop(0.27, Color.GREEN),
                new Stop(0.30, Color.YELLOW),
                new Stop(0.33, Color.ORANGE),
                new Stop(1.0, Color.RED)));
        contourRenderer.setNumberQuantisationLevels(100);
        contourRenderer.setMaxContourSegments(10);
        contourRenderer.setMinRequiredReductionSize(1);
        contourRenderer.setPointReduction(true);
        contourRenderer.setReductionType(ReductionType.MAX);
        contourRenderer.shiftZAxisToLeft();
        contourRenderer.gradientRect.setTranslateX(35);
        contourRenderer.gradientRect.setOpacity(0.5);
        contourRenderer.gradientRect.setWidth(35);


        this.getRenderers().setAll(contourRenderer);
        contourRenderer.getDatasets().add(dataSet);

        CustomZoomer zoomer = new CustomZoomer();
        zoomer.setAutoZoomEnabled(true);
        zoomer.setAddButtonsToToolBar(true);
        zoomer.setSliderVisible(true);
        zoomer.setAxisMode(AxisMode.X);
        zoomer.setZoomInMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED && mouseEvent.getClickCount() == 1);
        zoomer.setZoomOriginMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getClickCount() == 2);
        zoomer.setZoomOutMouseFilter(mouseEvent -> false);
        zoomer.setZoomRange(1500,30000);
        this.getPlugins().add(zoomer);
    }

    public void setZAxis(double max)
    {
        zAxis.set(-120,max);
    }

    public void setWaterfallGradient(double threshold)
    {
        Axis axisZ = this.getAxes().get(2);

        double startGrad = 1 - ((Math.abs(threshold) + Math.abs(axisZ.getMax())) / (Math.abs(axisZ.getMin()) + Math.abs(axisZ.getMax())));

        double stopGrad = startGrad + 0.18;

        if(stopGrad > 1.0)
            stopGrad = 0.9;

        ((ContourDataSetRendererCustom)this.getRenderers().get(0)).setColorGradient(new ColorGradient("CUSTOM",
                new Stop(0.0, Color.TRANSPARENT),
                new Stop(0.1, Color.DARKBLUE),
                new Stop(startGrad - 0.015, Color.BLUE),
                new Stop(startGrad + 0.01, Color.GREEN),
                new Stop(startGrad + 0.04, Color.YELLOW),
                new Stop(stopGrad, Color.ORANGE),
                new Stop(1.0, Color.RED)));
    }

    public void setDataSet(byte[] data)
    {
        dataSet.setData(data);
    }
}