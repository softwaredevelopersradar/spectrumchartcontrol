package KvetkaSpectrumLib.Waterfall;

import KvetkaSpectrumLib.CustomRenders.ContourDataSetRendererCustom;
import KvetkaSpectrumLib.CustomStringConverter.CustomTickLabelFormatter;
import KvetkaSpectrumLib.CustomZoom.CustomZoomer;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.AxisMode;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.renderer.ContourType;
import de.gsi.chart.renderer.datareduction.ReductionType;
import de.gsi.chart.renderer.spi.utils.ColorGradient;
import de.gsi.chart.ui.geometry.Side;
import javafx.geometry.Insets;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

import java.util.List;


public class WaterfallChart extends XYChart {
    private double maximumXValue = 30000;
    private double minimumXValue = 1500;
    private double maximumYValue = 30;
    private double minimumYValue = -120;
    private final String crossIndicatorXName = "Частота";
    private final String axisXUnit = "кГц";
    private ColorGradient colorGradient = new ColorGradient("CUSTOM",
            new Stop(0.0, Color.TRANSPARENT),
            new Stop(0.255, Color.BLUE),
            new Stop(0.27, Color.GREEN),
            new Stop(0.30, Color.YELLOW),
            new Stop(0.33, Color.ORANGE),
            new Stop(1.0, Color.RED));


    private WaterfallDataSource dataSet;
    private DefaultNumericAxis zAxis;
    private ContourDataSetRendererCustom contourRenderer;


    public WaterfallChart()
    {
        super(new DefaultNumericAxis(),new DefaultNumericAxis());
        dataSet = new WaterfallDataSource();
        InitializeComponents();
        this.getAxesAndCanvasPane().getRowConstraints().get(2).setPrefHeight(50);
    }

    public void DataSetInit(int size)
    {
        dataSet.Init(size);
    }

    private void InitializeComponents() {
        this.setAnimated(false);
        this.getRenderers().clear();
        this.setLegendVisible(false);
        this.getPlotArea().setBorder(new Border(new BorderStroke(Color.WHITE,
                BorderStrokeStyle.SOLID,
                new CornerRadii(0d), new BorderWidths(0,0,0,1))));
        this.getAxesAndCanvasPane().setPadding(new Insets(0,0,0,54));


        InitializeAxes();
        InitializeRender();
        InitializeZoomer();
    }

    public void InitializeAxes()
    {
        this.getXAxis().setAutoRanging(false);
        this.getXAxis().setAnimated(false);
        this.getXAxis().setName(crossIndicatorXName);
        this.getXAxis().setUnit(axisXUnit);
        this.getXAxis().set(minimumXValue, maximumXValue);

        ((DefaultNumericAxis)this.getXAxis()).setTickLabelFormatter(new CustomTickLabelFormatter());

        this.getYAxis().setAnimated(false);
        this.getYAxis().setName("");
        this.getYAxis().setAutoRanging(true);
        this.getYAxis().setSide(Side.LEFT);
        this.getYAxis().set(0,-32);
        this.getYAxis().getCanvas().setWidth(0);
        ((DefaultNumericAxis)this.getYAxis()).setMaxWidth(0);
        ((DefaultNumericAxis)this.getYAxis()).setVisible(false);
        this.getYAxis().getCanvas().setVisible(false);

        zAxis = new DefaultNumericAxis();
        zAxis.setAnimated(false);
        zAxis.setAutoRangeRounding(false);
        zAxis.setAutoRanging(false);
        zAxis.setSide(Side.LEFT);
        zAxis.set(minimumYValue,maximumYValue);
    }

    public void InitializeRender()
    {
        contourRenderer = new ContourDataSetRendererCustom();
        contourRenderer.getAxes().add(this.getXAxis());
        contourRenderer.getAxes().add(this.getYAxis());
        contourRenderer.getAxes().add(zAxis);
        contourRenderer.setContourType(ContourType.HEATMAP);
        contourRenderer.setColorGradient(colorGradient);
        contourRenderer.setNumberQuantisationLevels(100);
        contourRenderer.setMaxContourSegments(100);
        contourRenderer.setMinRequiredReductionSize(1);
        contourRenderer.setPointReduction(true);
        contourRenderer.setReductionType(ReductionType.MAX);

        this.getRenderers().setAll(contourRenderer);
        contourRenderer.getDatasets().add(dataSet);

        setWaterfallGradient(-120, 20, true);
    }

    public void InitializeZoomer()
    {
        CustomZoomer zoomer = new CustomZoomer();
        zoomer.setSliderVisible(false);
        zoomer.setAddButtonsToToolBar(false);
        zoomer.setAutoZoomEnabled(false);
        zoomer.setAxisMode(AxisMode.X);
        zoomer.setZoomInMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED && mouseEvent.getClickCount() == 1);
        zoomer.setZoomOriginMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getClickCount() == 2);
        zoomer.setZoomOutMouseFilter(mouseEvent -> false);
        zoomer.setZoomRange(1500,30000);
        this.getPlugins().add(zoomer);
    }

    public void setZAxis(double max, boolean isExtend) {
        zAxis.set(-120, max);
        setWaterfallGradient(-120, max, isExtend);
    }

    public void setDataSet(double[] data) {
        dataSet.setData(data);
    }

    public void clearDataSet(){
        dataSet.clearDataSet();
    }

    public DefaultNumericAxis getzAxis() { return zAxis; }

    public LinearGradient getColorGradient() {return new LinearGradient(0,1,0,0,true, CycleMethod.NO_CYCLE, colorGradient.getStops());}

    public int getFrameSize() { return dataSet.getFrameSize();}


    public void setWaterfallGradient(double min, double max, boolean isExtend)
    {
        List<Stop> stops = colorGradient.getStops();

        double startGrad = (stops.get(2).getOffset() * (Math.abs(min) + Math.abs(max))) / (Math.abs(min) + (Math.abs(max) + 10 * (isExtend ? 1.0 : -1.0)));
        if(startGrad >= 0.98) return;

        colorGradient = new ColorGradient("CUSTOM",
                 new Stop(0.0, Color.TRANSPARENT),
                 new Stop(startGrad - 0.0117, Color.TRANSPARENT),
                 new Stop(startGrad, Color.GREEN),
                 new Stop(startGrad + 0.03, Color.YELLOW),
                 new Stop(startGrad + 0.06, Color.ORANGE),
                 new Stop(1.0, Color.RED));
        contourRenderer.setColorGradient(colorGradient);

        gradientCoefficient = startGrad;

    }

    public void setWaterfallGradient_ByScrolling(double threshold)
    {
        double coef = 1.0/400.0 * (threshold < 0 ? -1.0 : 1.0);

        setGradientDependFromCoefficient_RainBow(coef);
    }


    private double gradientCoefficient;
    public double getGradientCoefficient()
    {
        return gradientCoefficient;
    }

    public boolean setGradientDependFromCoefficient_RainBow(double coef)
    {
        List<Stop> stops = colorGradient.getStops();
        double oldMainStopValue = stops.get(2).getOffset();
        double newMainStopValue = oldMainStopValue + coef;

        if(newMainStopValue < 0.02)
            coef = coef + (0.02 - newMainStopValue);
        if(newMainStopValue > 0.95)
            coef = coef -(newMainStopValue-0.95);

        colorGradient = new ColorGradient("CUSTOM",
                new Stop(0.0, Color.TRANSPARENT),
                new Stop(stops.get(1).getOffset() + coef, Color.TRANSPARENT),
                new Stop(stops.get(2).getOffset() + coef, Color.GREEN),
                new Stop(stops.get(3).getOffset() + coef, Color.YELLOW),
                new Stop(stops.get(4).getOffset() + coef, Color.ORANGE),
                new Stop(1.0, Color.RED));

        contourRenderer.setColorGradient(colorGradient);

        gradientCoefficient = stops.get(2).getOffset() + coef;

        return true;
    }

//    public boolean setGradientDependFromCoefficient(double coef, Color color)
//    {
//        List<Stop> stops = colorGradient.getStops();
//
//        if((stops.get(1).getOffset() + coef <= 0.02) || (stops.get(1).getOffset() + coef >= 0.95))
//            return false;
//
//        colorGradient = new ColorGradient("CUSTOM",
//                new Stop(0.0, Color.TRANSPARENT),
//                new Stop(stops.get(1).getOffset() + coef, Color.TRANSPARENT),
//                new Stop(stops.get(2).getOffset() + coef, color));
//
//        contourRenderer.setColorGradient(colorGradient);
//
//        gradientCoefficient = stops.get(1).getOffset() + coef;
//
//        return true;
//    }
//
//    public void setWaterfallGradient(double min, double max, boolean isExtend, Color color)
//    {
//        List<Stop> stops = colorGradient.getStops();
//
//        double startGrad = (stops.get(2).getOffset() * (Math.abs(min) + Math.abs(max))) / (Math.abs(min) + (Math.abs(max) + 10 * (isExtend ? 1.0 : -1.0)));
//
//        colorGradient = new ColorGradient("CUSTOM",
//                new Stop(0.0, Color.TRANSPARENT),
//                new Stop(startGrad - 0.0117, Color.TRANSPARENT),
//                new Stop(1.0, color));
//        contourRenderer.setColorGradient(colorGradient);
//
//        gradientCoefficient = startGrad;
//
//    }

}