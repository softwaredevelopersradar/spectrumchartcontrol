package KvetkaSpectrumLib.Waterfall;

import de.gsi.dataset.DataSet;
import de.gsi.dataset.GridDataSet;
import de.gsi.dataset.event.AddedDataEvent;
import de.gsi.dataset.spi.AbstractDataSet;
import de.gsi.math.ArrayUtils;
import it.unimi.dsi.fastutil.floats.FloatArrayList;

/**
 * DataSet source for testing real-time continuous 2D and 3D type data.
 *
 * @author rstein
 */
public class WaterfallDataSource extends AbstractDataSet<WaterfallDataSource> implements GridDataSet {
    private static final int INITIAL_FRAME_SIZE = 285000;
    private static final int INITIAL_FRAME_COUNT = 32;

    protected FloatArrayList[] history = {
            new FloatArrayList(INITIAL_FRAME_SIZE), // xValues
            new FloatArrayList(INITIAL_FRAME_COUNT), // yValues
            new FloatArrayList(INITIAL_FRAME_SIZE *INITIAL_FRAME_COUNT) // zValues
    };
    protected int circIndex = 0; // circular buffer index
    protected int frameSize = INITIAL_FRAME_SIZE;
    protected int frameCount = INITIAL_FRAME_COUNT;
    protected int updatePeriod = 100;

    public WaterfallDataSource() {
        // ToDo: add Enum to select
        // * RAW, FFT, MAG data
        // * initial sampling, binning, history depth
        super(WaterfallDataSource.class.getSimpleName(), 3);
        reinitializeData();
    }

    public void Init(int size)
    {
        this.frameSize = size;
        history = new FloatArrayList[]{
                new FloatArrayList(frameSize), // xValues
                new FloatArrayList(frameCount), // yValues
                new FloatArrayList(frameSize * frameCount)// zValues
        };
        reinitializeData();
    }


    @Override
    public double get(final int dimIndex, final int index) {
        if(history == null) return 0;

        if (dimIndex <= DIM_Y) {
            int dim = history[dimIndex].size();
            if(history[dimIndex].size() > 0)
                return history[dimIndex].getFloat(dimIndex == DIM_X ? index % frameSize : index / frameSize);
            return 0;
        }
        if(history[dimIndex].size() > 0)
            return history[dimIndex].getFloat((index + circIndex) % (frameSize * frameCount));
        return 0;

    }

    @Override
    public int getDataCount() {
        return frameCount * frameSize;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public int getFrameSize() {
        return frameSize;
    }

    public int getUpdatePeriod() {
        return updatePeriod;
    }

    @Override
    public double getValue(final int dimIndex, final double... x) {
        return 0;
    }

    public void clearDataSet()
    {
        reinitializeData();
    }

    protected void reinitializeData() {
        if(history != null)
        {
            history[DIM_X].size(frameSize);
            history[DIM_Y].size(frameCount);
            history[DIM_Z].size(frameSize * frameCount);
            circIndex = 0;

            double y = (30000.0-1500.0) / (double) frameSize;

            for (int i = 0; i < frameSize; i++) {
                history[DIM_X].elements()[i] = (float) (i * y + 1500);
            }
            for (int i = 0; i < frameCount; i++) {
                history[DIM_Y].elements()[i] = (float) (-0.001 * updatePeriod * i);
            }

            ArrayUtils.fillArray(history[DIM_Z].elements(), -120.0f);
        }
    }

    protected void setData(double[] data) {
        float[] waveform = new float[data.length];

        for (int i = 0; i < waveform.length; i++) {
               waveform[i] = (float)data[i];
        }

        System.arraycopy(waveform, 0, history[DIM_Z].elements(), circIndex, waveform.length);

        circIndex = (circIndex + frameSize);
        if(circIndex >= history[DIM_Z].elements().length)
        {
            circIndex = 0;
        }


        fireInvalidated(new AddedDataEvent(WaterfallDataSource.this, "new frame"));
    }

    @Override
    public int[] getShape() {
        return new int[] { frameSize, frameCount };
    }

    @Override
    public double getGrid(int dimIndex, int index) {
        return history[dimIndex].getFloat(index);
    }

    @Override
    public int getGridIndex(final int dimIndex, final double x) {
        if (dimIndex >= getNGrid()) {
            throw new IndexOutOfBoundsException("dim index out of bounds");
        }
        if (getShape(dimIndex) == 0) {
            return 0;
        }

        if (!Double.isFinite(x)) {
            return 0;
        }

        if (x <= this.getAxisDescription(dimIndex).getMin()) {
            return 0;
        }

        final int lastIndex = getShape(dimIndex) - 1;
        if (x >= this.getAxisDescription(dimIndex).getMax()) {
            return lastIndex;
        }

        // binary closest search -- assumes sorted data set
        return binarySearch(x, 0, lastIndex, i -> getGrid(dimIndex, i));
    }

    @Override
    public double get(int dimIndex, int... indices) {
        switch (dimIndex) {
            case DIM_X:
            case DIM_Y:
                return history[dimIndex].getFloat(indices[dimIndex]);
            case DIM_Z:
                return history[DIM_Z].getFloat(((indices[DIM_X] + frameSize * indices[DIM_Y]) + circIndex) % (frameSize * frameCount));
            default:
                throw new IndexOutOfBoundsException("dimIndex out of bound 3");
        }
    }

    @Override
    public DataSet set(final DataSet other, final boolean copy) {
        throw new UnsupportedOperationException("copy setting transposed data set is not implemented");
    }
}