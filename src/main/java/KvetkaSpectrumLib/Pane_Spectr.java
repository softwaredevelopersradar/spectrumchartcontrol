package KvetkaSpectrumLib;


import KvetkaModels.AnalogReconFWSModel;
import KvetkaSpectrumLib.Drawing.DrawArea;
import KvetkaSpectrumLib.Interfaces.*;
import KvetkaSpectrumLib.Models.SignalOnTrackingModel;
import KvetkaSpectrumLib.OtherControls.Attenuators;
import KvetkaSpectrumLib.OtherControls.Filters;
import KvetkaSpectrumLib.Signal.SignalButton;
import KvetkaSpectrumLib.Signal.SignalParameters;
import de.gsi.chart.axes.Axis;
import de.gsi.dataset.DataSet;
import de.gsi.dataset.utils.ByteArrayCache;
import de.gsi.dataset.utils.DoubleArrayCache;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadListener;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Класс графика спектра
 * @autor Ягелло Александр, инженер-программист
 *
 *
 * Чтобы получить доступ к контролу аттенюаторов используйте функцию <code>getAttenuatorsControl()<code/>
 * Чтобы получить доступ к контролу фильтров используйте функцию <code>getFilersControl()<code/>
 * @version 1.0
 */
public class Pane_Spectr extends AnchorPane implements Initializable, ICallBackFrequencyChangedInSubSpectrum {
    @FXML
    private Button _1_1btn;

    @FXML
    private Button moveLeftbtn;

    @FXML
    private Button moveRightbtn;

    @FXML
    private Button movePlusbtn;

    @FXML
    private Button moveZoombtn;

    @FXML
    private OnlyChart onlyChart;

    @FXML
    private Label thresholdLabel;

    @FXML
    private Label frequencyLabel;

    @FXML
    private Label levelLabel;

    @FXML
    private HBox HBoxWithSignalButtons;

    @FXML
    private PartSpectr onlyOneSignalSpectr;

    @FXML
    private Attenuators attenuators;

    @FXML
    private Filters filters;

    @FXML
    private WaterfallPanel waterfallPanel;

    @FXML
    private SplitPane splitVerticalChart;

    @FXML
    private Slider timeSlider;

    @FXML
    private Label startSliderLabel;

    @FXML
    private Label currentValueTimeSlider;

    @FXML
    private Label nowLabel;
    @FXML
    private Circle spectrumOnlineCircle;

    private final int minZoomRange = 1500;
    private final int maxZoomRange = 30000;

    private double porog = -80;
    private double deltaComparison = 0.005;
    private int IdCounter = 0;
    private int btnId = -1; //flag
    private ICallBackSignalEvents iCallBackSignalEvents;
    private ICallBackTimeSlider iCallBackTimeSlider;
    private ICallBackCommonChartEvents iCallBackCommonChartEvents;
    private ICallBackAthenuators iCallBackAthenuators;
    private ICallBackFilters iCallBackFilters;
    private ICallBackSound iCallBackSound;
    private double[] waterfallDataSet = new double[0];
    private Timer myTimer;
    private int setArraySize = 0;
    private boolean isTimerStart = false;

    /**
     * Creates a new instance of <code>Pane_Spectr</code>
     * @throws IOException
     */
    public Pane_Spectr() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Spectrum.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        onlyChart.getPlotArea().setOnMouseClicked(this::handleClickMouseEvent);
        onlyChart.setXTranslation(0);

        onlyOneSignalSpectr.registerCallBackCommonChartEvents(this);


        waterfallPanel.setOnScroll(new EventHandler<ScrollEvent>() {
               @Override
               public void handle(ScrollEvent scrollEvent) {
                   if(scrollEvent.isControlDown())
                   {
                       double coeff = scrollEvent.getDeltaY();
                       waterfallPanel.setCoeffGradient(coeff);
                   }
               }
           }
        );
    }

    public void startWaterfallTimer()
    {
        if(!isTimerStart)
        {
            myTimer = new Timer();
           TimerTask timerTask = new TimerTask() {
                public void run() {
                    Platform.runLater(() -> {
                        if(waterfallDataSet == null && waterfallDataSet.length < 0) return;

                        waterfallDataSet = Arrays.copyOfRange(onlyChart.getYList(), 0, waterfallPanel.getWaterfallChart().getFrameSize());

                        for(int i = 100; i < 200; i++)
                        {
                            if(waterfallDataSet[i] != 0)
                            {
                                waterfallPanel.setDataSet(waterfallDataSet);
                                break;
                            }
                        }

                    });
                }
            };
            myTimer.schedule(timerTask, 100, 100);
            isTimerStart = true;
        }
    }

    public void stopWaterfallTimer()
    {
        myTimer.cancel();
        myTimer.purge();
        myTimer = null;
        isTimerStart = false;
    }
    /**
     * initialize handles for indicators (cross and threshold indicator)
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        onlyChart.setZoomRange(minZoomRange, maxZoomRange);
        onlyChart.setZoomDepth(10);

        onlyChart.getXAxis().minProperty().bindBidirectional(waterfallPanel.getXAxis().minProperty());
        onlyChart.getXAxis().maxProperty().bindBidirectional(waterfallPanel.getXAxis().maxProperty());

        levelLabel.setText("U = "
                .concat(String.valueOf(Math.round(onlyChart.getValueY_Cross() * 10)/10.0))
                .concat(" дБ   "));
        thresholdLabel.setText("h (порог) = "
                .concat(String.valueOf(Math.round(onlyChart.getValue_Porog()* 10)/10.0))
                .concat(" дБ   "));
        frequencyLabel.setText("F = "
                .concat(String.valueOf((Math.round(onlyChart.getValueX_Cross()*1000)/1000.0)))
                .concat(" кГц   "));

        onlyChart.getValueProperty_Porog().addListener((observableValue, number, t1) -> {
            onlyOneSignalSpectr.setLevelValue(onlyChart.getValue_Porog());
            porog = (double) Math.round(onlyChart.getValue_Porog() * 10) / 10.0;
            thresholdLabel.setText("h (порог) = "
                    .concat(String.valueOf(porog))
                    .concat(" дБ   "));
            iCallBackCommonChartEvents.thresholdChanged(porog);
        });

        onlyChart.getValuePropertyX_Cross().addListener((observableValue, number, t1) -> frequencyLabel.setText("F = "
                .concat(String.valueOf(Math.round(onlyChart.getValueX_Cross() * 1000) / 1000.0))
                .concat(" кГц   ")));

        onlyChart.getValuePropertyY_Cross().addListener((observableValue, number, t1) -> levelLabel.setText("U = "
                .concat(String.valueOf(Math.round(onlyChart.getValueY_Cross() * 10) / 10.0))
                .concat(" дБ   ")));

        AttenuatorButtonsEvent();
        FiltersButtonsEvent();
        SoundControllEvent();





        timeSlider.setMax(0);
        setMinTimeSlider(-300);


        Text text = new Text();
        text.setText("0 мин.");
        text.resize(100, 20);
        text.setStyle("-fx-fill: white;");
        nowLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                timeSlider.setValue(0);
                text.setText("0 мин.");
                iCallBackTimeSlider.SpectrumRequest(0);
                spectrumOnlineCircle.setStyle("-fx-fill: red");
            }
        });

        timeSlider.skinProperty().addListener((obs,old,skin)->{
            if(skin!=null){
                StackPane thumb = (StackPane)timeSlider.lookup(".thumb");
                StackPane track = (StackPane)timeSlider.lookup(".track");
                track.setStyle("-fx-background-color: linear-gradient(to right, #2D819D 100%, #161616 100%);");
                thumb.setPadding(new javafx.geometry.Insets(1,5,1,5));
                thumb.setPrefWidth(80);
                thumb.setMinWidth(500);
                thumb.getChildren().add(text);
            }
        });

        timeSlider.setOnMouseReleased(event -> {
            int timeInSeconds = (int)timeSlider.getValue();
            iCallBackTimeSlider.SpectrumRequest(timeInSeconds);

            if (timeInSeconds < 0)
                spectrumOnlineCircle.setStyle("-fx-fill: grey;");
            else spectrumOnlineCircle.setStyle("-fx-fill: red;");
        });


        timeSlider.valueProperty().addListener(new ChangeListener<>() {

            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                StackPane track = (StackPane)timeSlider.lookup(".track");
                int timeInSeconds = (int)timeSlider.getValue();
                int trackSelectionRange = 100 - (int)(Math.abs(timeInSeconds/timeSlider.getMin()) * 100);

                text.setText("-"+ Math.abs(timeInSeconds)/60+ " мин. "+ (timeInSeconds % 60 == 0 ? "" : (Math.abs(timeInSeconds) % 60 + " с.")));

                String style = String.format("-fx-background-color: linear-gradient(to right, #2D819D %d%%, #161616 %d%%);", trackSelectionRange,trackSelectionRange);
                track.setStyle(style);
            }
        });


    }


    /**
     * displays a new dataset (start frequency equals 1500, end frequency equals 30000)
     *
     * @param newDataSet array of level values
     */
    public void UpdateSpectr(byte[] newDataSet)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;

        setSetArraySize(newDataSet.length);
        onlyChart.PlotSpectr(newDataSet);
        UpdateSubSpectr();

        ClearCash();
    }

    /**
     * displays a new dataset (start frequency equals 1500)
     *
     * @param newDataSet array of level values
     * @param endFreq end frequency of the displayed spectrum
     */
    public void UpdateSpectr(byte[] newDataSet, double endFreq)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;

        setSetArraySize(newDataSet.length);
        onlyChart.PlotSpectr(endFreq, newDataSet);
        onlyChart.setMaximumXValue(endFreq);
        UpdateSubSpectr();

        ClearCash();
    }

    private void setSetArraySize(int arraySize)
    {
        if(isTimerStart && setArraySize != arraySize)
        {
            InitSizeWaterfallDataSet(arraySize);
            setArraySize = arraySize;
        }
    }


    public void InitSizeWaterfallDataSet(int size)
    {
        waterfallPanel.setDataSetSize(size);
    }

    /**
     * displays a new dataset
     *
     * @param newDataSet array of level values
     * @param startFreq start frequency of the displayed spectrum
     * @param endFreq end frequency of the displayed spectrum
     */
    public void UpdateSpectr(byte[] newDataSet, double startFreq, double endFreq)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;

        setSetArraySize(newDataSet.length);
        onlyChart.PlotSpectr(startFreq, endFreq, newDataSet);

        UpdateSubSpectr();

        ClearCash();
    }

    public void UpdateSpectr(byte[][] newDataSet, double[] startFreq, double[] endFreq)
    {
        if(newDataSet == null || startFreq == null || endFreq == null || startFreq.length != endFreq.length)
            return;
        onlyChart.ClearSpectr();

        onlyChart.PlotSpectr(startFreq, endFreq, newDataSet);
        UpdateSubSpectr();

        ClearCash();
    }

    public void ClearSpectrPart(double startFreq, double endFreq)
    {
        onlyChart.ClearSpectrPart(startFreq, endFreq);
        UpdateSubSpectr();
    }

    public void UpdateSubSpectrum(byte[] data, double startFreq, double endFreq)
    {
        onlyOneSignalSpectr.UpdateSpectr(data,startFreq,endFreq);
    }


    /**
     * get main chart object
     *
     */
    public OnlyChart getChart()
    {
        return onlyChart;
    }

    public Attenuators getAttenuatorsControl()
    {
        return attenuators;
    }

    public Filters getFiltersControl()
    {
        return filters;
    }

    public DrawArea getDrawingArea() {return onlyChart.getDrawArea();}

    public PartSpectr getSmallChart(){ return onlyOneSignalSpectr;}



    /**
     * change min pixel distance for reduction algorithm
     *
     * @param minPixelDistance The larger the value, the faster the output works, the smaller the more correct the values will be displayed
     */
    public void setMinPixelDistanceForReductionAlgorithm(int minPixelDistance)
    {
        onlyChart.setMinPixelDistanceForReductionAlgorithm(minPixelDistance);
    }

    /**
     * set a number that means which elements from the data set will be selected for display
     *
     * @param value every <code>count<code/> element selected from data set for display on chart
     */
    public void setSelectEveryValueCount(int value)
    {
        onlyChart.setSelectEvery_Count(value);
    }

    public void registerCallBackTimeSlider(ICallBackTimeSlider iCallBackTimeSlider)
    {
        this.iCallBackTimeSlider = iCallBackTimeSlider;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackSignalEvents instance of class which implements <code>ICallBackSignalEvents_1<code/> interface
     */
    public void registerCallBackSignalEvents(ICallBackSignalEvents iCallBackSignalEvents)
    {
        this.iCallBackSignalEvents = iCallBackSignalEvents;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackCommonChartEvents instance of class which implements <code>ICallBackCommonChartEvents<code/> interface
     */
    public void registerCallBackCommonChartEvents(ICallBackCommonChartEvents iCallBackCommonChartEvents)
    {
        this.iCallBackCommonChartEvents = iCallBackCommonChartEvents;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackAthenuators instance of class which implements <code>ICallBackAthenuators<code/> interface
     */
    public void registerCallBackAthenuators(ICallBackAthenuators iCallBackAthenuators)
    {
        this.iCallBackAthenuators = iCallBackAthenuators;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackFilters instance of class which implements <code>ICallBackFilters<code/> interface
     */
    public void registerCallBackFilters(ICallBackFilters iCallBackFilters)
    {
        this.iCallBackFilters = iCallBackFilters;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackSound instance of class which implements <code>ICallBackSound<code/> interface
     */
    public void registerCallBackSound(ICallBackSound iCallBackSound)
    {
        this.iCallBackSound = iCallBackSound;
    }

    /**
     * get button for signal
     *
     *
     * @param id id of signal
     * @return button for signal
     */
    public SignalButton GetButtonObject(int id)
    {
        return findSignalButton(id);
    }

    /**
     * get button for signal (the frequency must be set in the range + - 3 kHz for the signal to be found)
     *
     *
     * @param freq freq of signal
     * @return button for signal
     */
    public SignalButton GetButtonObject(double freq)
    {
        for(Node item : HBoxWithSignalButtons.getChildren()) {
           SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            double realSignalFrequency = (signalButton).getSignalParameters().getFrequency();
            if (realSignalFrequency >= (freq - 3) && realSignalFrequency <= freq + 3)
                return signalButton;
        }
        return null;
    }


    /**
     * handler for one to one button
     */
    public void _1_1Button(javafx.event.ActionEvent actionEvent) {
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(onlyChart.getMinimumXValue(), onlyChart.getMaximumXValue());
        onlyChart.getXAxis().forceRedraw();
        waterfallPanel.getXAxis().setAutoRanging(false);
        waterfallPanel.getXAxis().set(onlyChart.getMinimumXValue(), onlyChart.getMaximumXValue());
        waterfallPanel.getXAxis().forceRedraw();
    }


    /**
     * handler of the button designed to shift the chart to the left
     */
    public void MoveLeftButton(ActionEvent actionEvent) {
        double diff = (onlyChart.getXAxis().getMax() - onlyChart.getXAxis().getMin())/4;
        double X1 = onlyChart.getXAxis().getMin() - diff;
        if(X1 < 1500) {
            X1 = 1500;
            diff = 0;
        }
        final double X2 = onlyChart.getXAxis().getMax() - diff;
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(X1, X2);
        onlyChart.getXAxis().forceRedraw();
        waterfallPanel.getXAxis().setAutoRanging(false);
        waterfallPanel.getXAxis().set(X1,X2);
        waterfallPanel.getXAxis().forceRedraw();
    }

    /**
     * handler of the button designed to shift the chart to the right
     */
    public void MoveRightButton(ActionEvent actionEvent) {
        double diff = (onlyChart.getXAxis().getMax() - onlyChart.getXAxis().getMin())/4;
        double X2 = onlyChart.getXAxis().getMax() + diff;
        if(X2 > 30000) {
            X2 = 30000;
            diff = 0;
        }
        final double X1 = onlyChart.getXAxis().getMin() + diff;
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(X1,X2);
        onlyChart.getXAxis().forceRedraw();
        waterfallPanel.getWaterfallChart().getXAxis().setAutoRanging(false);
        waterfallPanel.getWaterfallChart().getXAxis().set(X1,X2);
        waterfallPanel.getWaterfallChart().getXAxis().forceRedraw();
    }


    /**
     * handler of the button designed for the zoom in of the chart
     */
    public void PlusZoomButton(ActionEvent actionEvent) {
        final double half = (onlyChart.getXAxis().getMax()+ onlyChart.getXAxis().getMin())/2;
        final double diffmin = (half - onlyChart.getXAxis().getMin())*0.9;

        onlyChart.getXAxis().setAutoRanging(false);
        if(diffmin <= 0)
            return;

        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(half-diffmin,half+diffmin);
        onlyChart.getXAxis().forceRedraw();
        waterfallPanel.getXAxis().setAutoRanging(false);
        waterfallPanel.getXAxis().set(half-diffmin,half+diffmin);
        waterfallPanel.getXAxis().forceRedraw();
    }


    /**
     * handler of the button designed for the zoom out of the chart
     */
    public void MinusZoomButton(ActionEvent actionEvent) {
        final double half = (onlyChart.getXAxis().getMax()+ onlyChart.getXAxis().getMin())/2;
        final double diffmin = (half - onlyChart.getXAxis().getMin())*(1/0.9);
        double leftBorder = half-diffmin;
        double rightBorder = half+diffmin;
        onlyChart.getXAxis().setAutoRanging(false);
        if(diffmin <= 0)
            return;
        if(leftBorder < minZoomRange)
            leftBorder = minZoomRange;
        if(rightBorder > maxZoomRange)
            rightBorder = maxZoomRange;

        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(leftBorder, rightBorder);
        onlyChart.getXAxis().forceRedraw();
        waterfallPanel.getXAxis().setAutoRanging(false);
        waterfallPanel.getXAxis().set(leftBorder, rightBorder);
        waterfallPanel.getXAxis().forceRedraw();
    }

    /**
     * handler of the button ...
     */
    public void ExtendRangeButton(ActionEvent actionEvent) {
        double value = onlyChart.getYAxis().getMax();
        if(value > 120) return;
        value += 10;
        onlyChart.setMaximumYValue(value);
        onlyOneSignalSpectr.getChart().setMaximumYValue(value);
        waterfallPanel.setZAxis(value, true);
    }

    /**
     * handler of the button ...
     */
    public void NarrowRangeButton(ActionEvent actionEvent) {
        double value = onlyChart.getYAxis().getMax();
        if(value < 0) return;
        value -= 10;
        onlyChart.setMaximumYValue(value);
        onlyOneSignalSpectr.getChart().setMaximumYValue(value);
        waterfallPanel.setZAxis(value, false);
    }

    /**
     * handler of the button designed for delete selected signal
     */
    public void OnDeleteSignals(ActionEvent actionEvent)
    {
        for(Node item : new ArrayList<Node>(HBoxWithSignalButtons.getChildren()))
        {
            SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            SignalParameters signalParameters = signalButton.getSignalParameters();

            ILambdaAction isCheckedAction = () -> signalButton.getIsCheckedStatus();
            DeleteSignal(isCheckedAction, item, signalParameters);
        }
    }

    /**
     * handler of the button designed for delete all tracked signals
     */
    public void OnDeleteAllSignals(ActionEvent actionEvent)
    {
        for(Node item : new ArrayList<Node>(HBoxWithSignalButtons.getChildren()))
        {
            SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            SignalParameters signalParameters = signalButton.getSignalParameters();

            ILambdaAction isCheckedAction = () -> true;
            DeleteSignal(isCheckedAction, item, signalParameters);
        }
    }

    public List<SignalOnTrackingModel> GetTrackingSignalsModel()
    {
        ArrayList<SignalOnTrackingModel> signalOnTrackingModels = new ArrayList<SignalOnTrackingModel>();
        for(Node item : new ArrayList<Node>(HBoxWithSignalButtons.getChildren()))
        {
            SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            SignalParameters signalParameters = signalButton.getSignalParameters();
            signalOnTrackingModels.add(new SignalOnTrackingModel(signalParameters.getCenterFrequency(), Math.abs(signalParameters.getFrequencyAtWhichSelectedAreaEnds() - signalParameters.getFrequencyAtWhichSelectedAreaStarts())));
        }
        return signalOnTrackingModels;
    }

    public void SetTrackingSignalsModel(List<SignalOnTrackingModel> signalOnTrackingModels)
    {
        SignalParameters signalParameters = null;
        for (SignalOnTrackingModel model: signalOnTrackingModels) {
            signalParameters = AddSignalOnTracking(model.Frequency, this.getChart().getAllDatasets().get(0), model.Range/2.0);
        }
        if(signalParameters == null) return;

        iCallBackSignalEvents.recuestSubSpectrum(signalParameters.getFrequencyAtWhichSelectedAreaStarts(), signalParameters.getFrequencyAtWhichSelectedAreaEnds());
        UpdateSubSpectr();
        onlyOneSignalSpectr.setFrequencyInSoundControl(signalParameters.getCenterFrequency(), signalParameters.getId());
        onlyOneSignalSpectr.setRange(signalOnTrackingModels.get(signalOnTrackingModels.size()-1).Range);

    }


    public void OnWaterfallOpen(ActionEvent actionEvent)
    {
       if(!waterfallPanel.isVisible())
       {
           waterfallPanel.setVisible(true);
           waterfallPanel.setMaxHeight(900);
           waterfallPanel.setPrefHeight(260);
           splitVerticalChart.setDividerPosition(0,0.6);
           waterfallPanel.getZAxis().forceRedraw();
           startWaterfallTimer();
       }
       else
       {
           waterfallPanel.setVisible(false);
           waterfallPanel.setMaxHeight(0);
           waterfallPanel.clearDataSet();
           stopWaterfallTimer();
       }
    }

    public void setMinTimeSlider(int timeInSeconds)
    {
        timeSlider.setMin(timeInSeconds);
        startSliderLabel.setText(timeInSeconds/60+ " мин. "+ (timeInSeconds % 60 == 0 ? "" : (Math.abs(timeInSeconds) % 60 + " с.")));
    }


    private void DeleteSignal(ILambdaAction deleteRule, Node item, SignalParameters signalParameters)
    {
        if(deleteRule.ActionBoolean())
        {
            iCallBackSignalEvents.signalDeleted(signalParameters.getFrequency(), signalParameters.getAnalogReconFWSModel());
            someSignalParametersChanged(signalParameters.getAnalogReconFWSModel(), false);
            onlyChart.deleteRangeIndicator(signalParameters.getCenterFrequency());
            HBoxWithSignalButtons.getChildren().remove(item);
            System.gc();
        }


        if(HBoxWithSignalButtons.getChildren().size() == 0)
        {
            HBoxWithSignalButtons.getChildren().clear();
            btnId = -1;
            onlyOneSignalSpectr.DefaultsLabels();
        }else {
            SignalButton signalButton0 = (SignalButton) HBoxWithSignalButtons.getChildren().get(0);
            btnId = signalButton0.getSignalId();
            SignalParameters signalParameters0 = signalButton0.getSignalParameters();
            onlyChart.changeColorXRangeIndicator(signalParameters0.getCenterFrequency());
        }
    }





    /**
     * displays a sub spectrum on small chart on the right side of control

     */
    private void UpdateSubSpectr()
    {
        double[] y = onlyChart.getYList();
        isSignalsExist(y);

        if(btnId != -1)
        {
            SignalButton signalButton = findSignalButton(btnId);
            if(signalButton == null) return;
            SignalParameters signalParameters = signalButton.getSignalParameters();
            final int startIndex = onlyChart.getAllDatasets().get(0).getIndex(0, signalParameters.getFrequencyAtWhichSelectedAreaStarts());
            final int endIndex = onlyChart.getAllDatasets().get(0).getIndex(0, signalParameters.getFrequencyAtWhichSelectedAreaStarts());

            if(signalParameters.getDataSet() == null || (startIndex == 0 && endIndex == 0))
                return;

            double[] subSpectrum = Arrays.copyOfRange(y, startIndex, endIndex);
            signalParameters.setDataSet(subSpectrum);
        }


    }

    /**
     * create new button for signal
     *
     *
     * @param signalParameters object which contains information about the signal
     * @return button for signal
     */
    private SignalButton CreateButton(SignalParameters signalParameters) throws IOException {
        SignalButton btn = new SignalButton();
        btn.setSignalId(IdCounter);
        btn.setSignalParameters(signalParameters);
        btn.setOnMouseClicked(event -> {
            int id = ((SignalButton)event.getSource()).getSignalId();
            btnId = id;
            ChoseSignalButton(id);
        });
        return btn;
    }

    public void AddSignalOnTracking(double freq, double deltaFreq)
    {
        var signal = AddSignalOnTracking(freq, this.getChart().getAllDatasets().get(0), deltaFreq);
        if(signal == null) return;
        iCallBackSignalEvents.recuestSubSpectrum(signal.getFrequencyAtWhichSelectedAreaStarts(), signal.getFrequencyAtWhichSelectedAreaEnds());
        UpdateSubSpectr();
    }

    public void SetFrequencyOnSoundControl(double freq)
    {
        double line = onlyOneSignalSpectr.getSelectedItemLine() / 1000.0;
        double delta = line/2.0;
        AddSignalOnTracking(freq, delta);
    }

    public void ChoseSignalOnTracking(int id)
    {
        ChoseSignalButton(id);
    }

    private void ChoseSignalButton(int id)
    {
        SignalParameters signal_Parameters = findSignalButton(id).getSignalParameters();
        onlyChart.changeColorXRangeIndicator(signal_Parameters.getCenterFrequency());
        iCallBackSignalEvents.recuestSubSpectrum(signal_Parameters.getFrequencyAtWhichSelectedAreaStarts(), signal_Parameters.getFrequencyAtWhichSelectedAreaEnds());
        onlyOneSignalSpectr.setPlayButtonAsNotSignal();
        onlyOneSignalSpectr.setFrequencyInSoundControl(signal_Parameters.getFrequency(), signal_Parameters.getId());
        someSignalParametersChanged(signal_Parameters.getAnalogReconFWSModel(), false);
        UpdateSubSpectr();
    }
    /**
     * checks for the presence of a signal set for tracking
     *
     * @param dataset array of level values
     */
    private void isSignalsExist(double[] dataset)
    {
        for(Node item : HBoxWithSignalButtons.getChildren())
        {
            SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            SignalParameters signalParameters = signalButton.getSignalParameters();

            final int startIndex = onlyChart.getAllDatasets().get(0).getIndex(0, signalParameters.getFrequencyAtWhichSelectedAreaStarts());//signalParameters.getIndexAtWhichSelectedAreaStarts();
            final int endIndex = onlyChart.getAllDatasets().get(0).getIndex(0, signalParameters.getFrequencyAtWhichSelectedAreaEnds());//signalParameters.getIndexAtWhichSelectedAreaEnds();
            double[] subSpectrum = Arrays.copyOfRange(dataset, startIndex, endIndex);
            signalParameters.setDataSet(subSpectrum);

            boolean deleteFlag = isSignalExist(startIndex, endIndex, dataset);

            if(!deleteFlag && signalParameters.isSignalExist())
            {
                signalButton.setIndicatorOfExistSignal(false);
                iCallBackSignalEvents.signalBecameUnavailable(signalButton.getSignalParameters().getFrequency(), signalButton.getAnalogReconFWSModel());
            }
            else if (deleteFlag && !signalParameters.isSignalExist())
            {
                signalButton.setIndicatorOfExistSignal(true);
                iCallBackSignalEvents.signalBecameAvailableAgain(signalParameters.getFrequency(), signalButton.getAnalogReconFWSModel());
                signalButton.setFreq(signalParameters.getFrequency());
            }
        }
    }

    private boolean isSignalExist(double[] dataset)
    {
      return isSignalExist(0,dataset.length,dataset);
    }

    private boolean isSignalExist(int indexAtWhichSelectedAreaStarts, int indexAtWhichSelectedAreaEnds, double[] dataset)
    {
        boolean deleteFlag = false;
        for(int i = indexAtWhichSelectedAreaStarts + 1; i < indexAtWhichSelectedAreaEnds; i++)
        {
             if(dataset[i - 1] < porog && dataset[i] >= porog)
            {
                while (i < indexAtWhichSelectedAreaEnds - 1)
                {
                    if(dataset[i] >= porog)
                    {
                        deleteFlag = true;
                        break;
                    }
                    i++;
                }
                break;
            }
        }

        return deleteFlag;
    }

    /**
     * handle double click on chart
     */
    private void handleClickMouseEvent(MouseEvent mouseEvent) {
        //handle double click event.
        if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
            mouseEvent.consume();

            // getting mouse coordinate
            Point2D mouseClickCord = onlyChart.getValuePosFromMouseClick(mouseEvent.getSceneX(), mouseEvent.getSceneY());
            double clickXCoord = mouseClickCord.getX();

            var signal = AddSignalOnTracking(clickXCoord, this.getChart().getAllDatasets().get(0), onlyOneSignalSpectr.getSelectedItemLine()/2000.0);
            if(signal == null) return;
            iCallBackSignalEvents.recuestSubSpectrum(signal.getFrequencyAtWhichSelectedAreaStarts(), signal.getFrequencyAtWhichSelectedAreaEnds());
            UpdateSubSpectr();
            onlyOneSignalSpectr.setFrequencyInSoundControl(clickXCoord, signal.getId());
        }
    }

    private SignalParameters AddSignalOnTracking(double middleFrequencyOfTrackingZone, DataSet dataSet, double deltaFreq) {

        double startFreqOfSelectedArea = middleFrequencyOfTrackingZone - deltaFreq;
        double endFreqOfSelectedArea = middleFrequencyOfTrackingZone + deltaFreq;

        double[] y;
        int startIndexOfSelectedArea;
        int endIndexOfSelectedArea;

//        if(dataSet.getValues(0).length <= 5)
//        {
//            startFreqOfSelectedArea = 0;
//            endIndexOfSelectedArea = 0;
//            y = null;
//        }

        y = dataSet.getValues(1);

        startIndexOfSelectedArea = dataSet.getIndex(0,startFreqOfSelectedArea);
        endIndexOfSelectedArea = dataSet.getIndex(0,endFreqOfSelectedArea);


        if(isSignalConsistByBounds(startFreqOfSelectedArea, endFreqOfSelectedArea, deltaFreq) == -1) {//check if a signal has met such parameters before
            onlyChart.addXRangeIndicator(startFreqOfSelectedArea, endFreqOfSelectedArea);

            double[] subSpectrum = null;
            boolean isSignalExist = false;
            if(y != null)
            {
                subSpectrum = Arrays.copyOfRange(y, startIndexOfSelectedArea, endIndexOfSelectedArea);
                isSignalExist = isSignalExist(subSpectrum);
            }


            IdCounter++;
            SignalParameters signalParameters = new SignalParameters(IdCounter,subSpectrum, startIndexOfSelectedArea, endIndexOfSelectedArea, startFreqOfSelectedArea, endFreqOfSelectedArea, isSignalExist);

            try{HBoxWithSignalButtons.getChildren().add(CreateButton(signalParameters));}
            catch (Exception ex) {
                int i = 0;
                i++;
            }

            btnId = IdCounter;

            AnalogReconFWSModel analogReconFWSModel = signalParameters.getAnalogReconFWSModel();
            analogReconFWSModel.setId(btnId);
            iCallBackSignalEvents.signalAdded(signalParameters.getFrequency(), analogReconFWSModel);
            someSignalParametersChanged(analogReconFWSModel, false);
            onlyOneSignalSpectr.setPlayButtonAsNotSignal();


            return signalParameters;
        }
        return  null;
    }

    private SignalButton findSignalButton(int id)
    {
        for(Node signalButton : HBoxWithSignalButtons.getChildren())
        {
            if(((SignalButton)signalButton).getSignalId() == id)
                return (SignalButton)signalButton;
        }
        return null;
    }

    private int isSignalConsistByBounds(double leftBound, double rightBound, double delta)
    {
        for(Node item : HBoxWithSignalButtons.getChildren())
        {
            SignalParameters signalParameters = ((SignalButton)item).getSignalParameters();

            final double somethingLeftBound = signalParameters.getFrequencyAtWhichSelectedAreaStarts();
            final double somethingRightBound = signalParameters.getFrequencyAtWhichSelectedAreaEnds();
            if(somethingLeftBound <= leftBound + delta && somethingLeftBound >= leftBound - delta && somethingRightBound <= rightBound + delta && somethingRightBound >= rightBound - delta)
                return ((SignalButton)item).getSignalId();
        }
        return -1;
    }


    private void AttenuatorButtonsEvent()
    {
        attenuators.getSelectionModelListener().addListener((options, oldValue, newValue) -> {
            int selectedItem = Integer.parseInt(attenuators.getSelectedItem());
            if(selectedItem != -1)
                iCallBackAthenuators.buttonAttenuatorSelected(selectedItem);
        });
    }

    private void FiltersButtonsEvent()
    {
        filters.getSelectionModelListener().addListener((options, oldValue, newValue) -> {
            double selectedItem = Double.parseDouble(filters.getSelectedItem());
            if(selectedItem != -1)
                iCallBackFilters.buttonFilterSelected(selectedItem);
        });
    }


    @Override
    public void FrequencyChanged(double oldValue, double newValue, int idOfSelectedSignalButton) {
        if(newValue <=0 ) return;
        if(newValue < minZoomRange || newValue > maxZoomRange) return;

        double line = onlyOneSignalSpectr.getSelectedItemLine() / 1000.0;
        double delta = line/2.0;



        if(Math.abs(oldValue - newValue) > deltaComparison)
        {
            for(Node item : new ArrayList<Node>(HBoxWithSignalButtons.getChildren()))
            {
                SignalButton signalButton = (SignalButton)item;
                if(signalButton == null) continue;
                SignalParameters signalParameters = signalButton.getSignalParameters();

                double tt = Math.abs(signalParameters.getFrequency() - newValue);
                ILambdaAction isCheckedAction = () -> Math.abs(signalParameters.getFrequency() - oldValue) < deltaComparison ;
                if(isCheckedAction.ActionBoolean())
                {
                    iCallBackSignalEvents.signalDeleted(signalParameters.getFrequency(), signalParameters.getAnalogReconFWSModel());
                    someSignalParametersChanged(signalParameters.getAnalogReconFWSModel(), false);
                    onlyChart.deleteRangeIndicator(signalParameters.getCenterFrequency());
                    HBoxWithSignalButtons.getChildren().remove(item);
                    System.gc();
                }


                //DeleteSignal(isCheckedAction, item, signalParameters);
            }
            AddSignalOnTracking(newValue, delta);
        }
        rangeOfSignalChanged(delta);
    }

    private void SoundControllEvent()
    {
        onlyOneSignalSpectr.getEventPlayButton().set(this::handleClickPlay);

        onlyOneSignalSpectr.getComboBoxLine().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                double line = onlyOneSignalSpectr.getSelectedItemLine() / 1000.0;
                if(line == -1.0 || s == null)
                    return;

                double freq = onlyOneSignalSpectr.getFrequency();
                double startFreqOfSelectedArea = freq - line / 2.0;
                double endFreqOfSelectedArea = freq + line / 2.0;

                DeleteSignalFromTracking(freq);

                rangeOfSignalChanged(line / 2.0);
            }
        });

        onlyOneSignalSpectr.getComboBoxModulation().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                AnalogReconFWSModel analogReconFWSModel = createAnalogReconFWSModel();
                DeleteSignalFromTracking(analogReconFWSModel.getFrequency());
                if(s == null)
                    return;
                someSignalParametersChanged(analogReconFWSModel, true);
            }
        });
    }

    private void DeleteSignalFromTracking(double value)
    {
        double line = onlyOneSignalSpectr.getSelectedItemLine() / 1000.0;
        double delta = line/2.0;

        for(Node item : new ArrayList<Node>(HBoxWithSignalButtons.getChildren()))
        {
            SignalButton signalButton = (SignalButton)item;
            if(signalButton == null) continue;
            SignalParameters signalParameters = signalButton.getSignalParameters();

            ILambdaAction isCheckedAction = () -> Math.abs(signalParameters.getFrequency() - value) < deltaComparison;
            DeleteSignal(isCheckedAction, item, signalParameters);
        }

        AddSignalOnTracking(value, delta);
    }

    private void rangeOfSignalChanged(double deltaFreq)
    {
        double freq = onlyOneSignalSpectr.getFrequency();
        double startFreqOfSelectedArea = freq - deltaFreq;
        double endFreqOfSelectedArea = freq + deltaFreq;

        SignalParameters signalParameters = new SignalParameters(startFreqOfSelectedArea, endFreqOfSelectedArea);
        if(btnId != -1)
        {
            final int startIndexOfSelectedArea = onlyChart.getAllDatasets().get(0).getIndex(0,startFreqOfSelectedArea);
            final int endIndexOfSelectedArea = onlyChart.getAllDatasets().get(0).getIndex(0,endFreqOfSelectedArea);
            SignalButton signalButton = findSignalButton(btnId);
            if(signalButton == null) return;
            signalParameters = signalButton.getSignalParameters();
            signalParameters.setFrequencyAtWhichSelectedAreaStarts(startFreqOfSelectedArea);
            signalParameters.setFrequencyAtWhichSelectedAreaEnds(endFreqOfSelectedArea);
            signalParameters.setIndexAtWhichSelectedAreaStarts(startIndexOfSelectedArea);
            signalParameters.setIndexAtWhichSelectedAreaEnds(endIndexOfSelectedArea);
            signalButton.setFreq(freq);

            onlyChart.updateXRangeIndicator(startFreqOfSelectedArea, endFreqOfSelectedArea, freq);
        }

        someSignalParametersChanged(signalParameters.getAnalogReconFWSModel(), true);
        UpdateSubSpectr();
    }

    private void someSignalParametersChanged(AnalogReconFWSModel analogReconFWSModel, boolean isStartPlaing)
    {
        if(getSmallChart().getPlayButton().isSelected())
        {
            analogReconFWSModel.setBand(onlyOneSignalSpectr.getSelectedItemLine().floatValue());
            analogReconFWSModel.setTypeSignal(onlyOneSignalSpectr.getSelectedItemModulation());
            iCallBackSound.Playing(analogReconFWSModel,isStartPlaing);
        }
    }

    private void handleClickPlay(ActionEvent actionEvent) {
        AnalogReconFWSModel analogReconFWSModel = createAnalogReconFWSModel();
        if(analogReconFWSModel.getId()<=0)
        {
            analogReconFWSModel.setId(0);
        }

        if(((ToggleButton)actionEvent.getSource()).isSelected())
        {
            iCallBackSound.Playing(analogReconFWSModel,true);
        }
        else {
            iCallBackSound.Playing(analogReconFWSModel, false);
        }
    }

    private AnalogReconFWSModel createAnalogReconFWSModel()
    {
        AnalogReconFWSModel analogReconFWSModel = new AnalogReconFWSModel();
        analogReconFWSModel.setFrequency(onlyOneSignalSpectr.getFrequency());
        analogReconFWSModel.setTypeSignal(onlyOneSignalSpectr.getSelectedItemModulation());
        analogReconFWSModel.setBand(onlyOneSignalSpectr.getSelectedItemLine().floatValue());
        SignalButton signalButton = findSignalButton(btnId);
        if(signalButton != null)
        {
            analogReconFWSModel.setId(signalButton.getSignalId());
        }

        return analogReconFWSModel;
    }

    private void ClearCash()
    {
        DoubleArrayCache.getInstance().clear();
        ByteArrayCache.getInstance().clear();
    }


}

