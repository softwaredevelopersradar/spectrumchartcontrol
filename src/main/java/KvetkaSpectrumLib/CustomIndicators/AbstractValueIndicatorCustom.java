package KvetkaSpectrumLib.CustomIndicators;

import de.gsi.chart.Chart;
import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.ChartPlugin;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

abstract class AbstractValueIndicatorCustom extends ChartPlugin {
    private final Axis axisX;
    private final Axis axisY;
    private final ChangeListener<? super Number> axisBoundsListener = (obs, oldVal, newVal) -> layoutChildren();

    private final ListChangeListener<? super ChartPlugin> pluginsListListener = (final ListChangeListener.Change<? extends ChartPlugin> change) -> updateStyleClass();

    private double xOffset;
    private double yOffset;

    protected final Label labelX = new Label();
    protected final Label labelY = new Label();
    protected final TextField labelEditX = new TextField();
    protected final TextField labelEditY = new TextField();

    public final Delta clickDelta = new Delta();

    protected final BooleanProperty editableIndicator = new SimpleBooleanProperty(this, "editableIndicator", true) {
        @Override
        protected void invalidated() {
            layoutChildren();
        }
    };

    private final ObjectProperty<HPos> labelHorizontalAnchor = new SimpleObjectProperty<HPos>(this,
            "labelHorizontalAnchor", HPos.CENTER) {
        @Override
        protected void invalidated() {
            layoutChildren();
        }
    };

    private final ObjectProperty<VPos> labelVerticalAnchor = new SimpleObjectProperty<VPos>(this, "labelVerticalAnchor",
            VPos.CENTER) {
        @Override
        protected void invalidated() {
            layoutChildren();
        }
    };

    protected AbstractValueIndicatorCustom(Axis axisX, Axis axisY, final String textX, final String textY) {
        super();
        this.axisX = axisX;
        this.axisY = axisY;
        setTextX(textX);
        setTextY(textY);

        labelX.pickOnBoundsProperty().set(true);
        labelX.toFront();

        labelX.setOnMousePressed(mouseEvent -> {
            if (mouseEvent.isPrimaryButtonDown() && isEditable()) {

                Point2D c = labelX.sceneToLocal(mouseEvent.getSceneX(), mouseEvent.getSceneY());
                clickDelta.x = -(c.getX() + xOffset);
                clickDelta.y = c.getY() + yOffset;
                mouseEvent.consume();
            }
        });

        labelY.pickOnBoundsProperty().set(true);
        labelY.toFront();

        labelY.setOnMousePressed(mouseEvent -> {
            if (mouseEvent.isPrimaryButtonDown() && isEditable()) {

                Point2D c = labelY.sceneToLocal(mouseEvent.getSceneX(), mouseEvent.getSceneY());
                clickDelta.x = -(c.getX() + xOffset);
                clickDelta.y = c.getY() + yOffset;
                mouseEvent.consume();
            }
        });

        chartProperty().addListener((obs, oldChart, newChart) -> {
            if (oldChart != null) {
                removeAxisListener();
                removePluginsListListener(oldChart);
            }
            if (newChart != null) {
                addAxisListener();
                addPluginsListListener(newChart);
            }
        });

        textPropertyX().addListener((obs, oldText, newText) -> layoutChildren());
    }

    private void addAxisListener() {
        final Axis valueAxisX = getAxisX();
        valueAxisX.minProperty().addListener(axisBoundsListener);
        valueAxisX.maxProperty().addListener(axisBoundsListener);

        final Axis valueAxisY = getAxisY();
        valueAxisY.minProperty().addListener(axisBoundsListener);
        valueAxisY.maxProperty().addListener(axisBoundsListener);
    }

    protected void addChildNodeIfNotPresent(final Node node) {
        if (!getChartChildren().contains(node)) {
            getChartChildren().add(0, node); // add elements always at the bottom so they cannot steal focus
        }
    }

    private void addPluginsListListener(final Chart chart) {
        chart.getPlugins().addListener(pluginsListListener);
        updateStyleClass();
    }

    public final BooleanProperty editableIndicatorProperty() {
        return editableIndicator;
    }

    private int getIndicatorInstanceIndex() {
        if (getChart() == null) {
            return 0;
        }
        final Class<?> thisClass = getClass();
        int instanceIndex = -1;
        for (final ChartPlugin plugin : getChart().getPlugins()) {
            if (plugin.getClass().equals(thisClass)) {
                instanceIndex++;
            }
            if (plugin == this) {
                break;
            }
        }
        return Math.max(instanceIndex, 0);
    }

    /**
     * Returns the value of the {@link #labelHorizontalAnchorProperty()}.
     *
     * @return value of the labelHorizontalAnchor property
     */
    public final HPos getLabelHorizontalAnchor() {
        return labelHorizontalAnchorProperty().get();
    }

    /**
     * Returns the value of the {@link #labelVerticalAnchorProperty()}.
     *
     * @return value of the labelVerticalAnchor property
     */
    public final VPos getLabelVerticalAnchor() {
        return labelVerticalAnchorProperty().get();
    }

    /**
     * Returns the Axis that this indicator is associated with.
     *
     * @return associated Axis
     */
    public Axis getAxisX() {
        return axisX;
    }
    public Axis getAxisY() {
        return axisY;
    }
    /**
     * Returns the value of the {@link #textPropertyX()}.
     *
     * @return text displayed within or next to the indicator
     */
    public final String getText() {
        return textPropertyX().get();
    }

    /**
     * @return whether this plugin is editable
     */
    public final boolean isEditable() {
        return editableIndicatorProperty().get();
    }

    /**
     * Specifies anchor of the {@link #textPropertyX() text label} with respect to the horizontal label position i.e. it
     * describes whether the position is related to the LEFT, CENTER or RIGHT side of the label. The position itself
     * should be specified by the extending classes.
     * <p>
     * <b>Default value: {@link HPos#CENTER}</b>
     * </p>
     *
     * @return labelHorizontalAnchor property
     */
    public final ObjectProperty<HPos> labelHorizontalAnchorProperty() {
        return labelHorizontalAnchor;
    }

    /**
     * Specifies anchor of the {@link #textPropertyX() text label} with respect to the vertical label position i.e. it
     * describes whether the position is related to the TOP, CENTER, BASELINE or BOTTOM of of the label. The position
     * itself should be specified by the extending classes.
     * <p>
     * <b>Default value: {@link VPos#CENTER}</b>
     * </p>
     *
     * @return labelVerticalAnchor property
     */
    public final ObjectProperty<VPos> labelVerticalAnchorProperty() {
        return labelVerticalAnchor;
    }

    /**
     * Layouts the label within specified bounds and given horizontal and vertical position, taking into account
     * {@link #labelHorizontalAnchorProperty() horizontal} and {@link #labelVerticalAnchorProperty() vertical} anchor.
     *
     * @param bounds the bounding rectangle with respect to which the label should be positioned
     * @param hPos relative [0, 1] horizontal position of the label within the bounds
     * @param vPos relative [0, 1] vertical position of the label within the bounds
     */
    protected final void layoutLabelX(final Bounds bounds, final double hPos, final double vPos) {
        if (labelX.getText() == null || labelX.getText().isEmpty()) {
            getChartChildren().remove(labelX);
            return;
        }

        double xPos = bounds.getMinX();
        double yPos = bounds.getMinY();

        xOffset = bounds.getWidth() * hPos;
        yOffset = bounds.getHeight() * (1 - vPos);

        final double width = labelX.prefWidth(-1);
        final double height = labelX.prefHeight(width);

        if (getLabelHorizontalAnchor() == HPos.CENTER) {
            xOffset -= width / 2;
        } else if (getLabelHorizontalAnchor() == HPos.RIGHT) {
            xOffset -= width;
        }

        if (getLabelVerticalAnchor() == VPos.CENTER) {
            yOffset -= height / 2;
        } else if (getLabelVerticalAnchor() == VPos.BASELINE) {
            yOffset -= labelX.getBaselineOffset();
        } else if (getLabelVerticalAnchor() == VPos.BOTTOM) {
            yOffset -= height;
        }

        labelX.resizeRelocate(xPos + xOffset, yPos + yOffset, width, height);
        addChildNodeIfNotPresent(labelX);
    }

    protected final void layoutLabelY(final Bounds bounds, final double hPos, final double vPos) {
        if (labelY.getText() == null || labelY.getText().isEmpty()) {
            getChartChildren().remove(labelY);
            return;
        }

        double xPos = bounds.getMinX();
        double yPos = bounds.getMinY();

        xOffset = bounds.getWidth() * hPos;
        yOffset = bounds.getHeight() * (1 - vPos);

        final double width = labelY.prefWidth(-1);
        final double height = labelY.prefHeight(width);

        if (getLabelHorizontalAnchor() == HPos.CENTER) {
            xOffset -= width / 2;
        } else if (getLabelHorizontalAnchor() == HPos.RIGHT) {
            xOffset -= width;
        }

        if (getLabelVerticalAnchor() == VPos.CENTER) {
            yOffset -= height / 2;
        } else if (getLabelVerticalAnchor() == VPos.BASELINE) {
            yOffset -= labelY.getBaselineOffset();
        } else if (getLabelVerticalAnchor() == VPos.BOTTOM) {
            yOffset -= height;
        }

        labelY.resizeRelocate(xPos + xOffset, yPos + yOffset, width, height);
         addChildNodeIfNotPresent(labelY);
    }

    private void removeAxisListener() {
        final Axis valueAxisX = getAxisX();
        valueAxisX.minProperty().removeListener(axisBoundsListener);
        valueAxisX.maxProperty().removeListener(axisBoundsListener);

        final Axis valueAxisY = getAxisY();
        valueAxisY.minProperty().removeListener(axisBoundsListener);
        valueAxisY.maxProperty().removeListener(axisBoundsListener);
    }

    private void removePluginsListListener(final Chart chart) {
        chart.getPlugins().removeListener(pluginsListListener);
    }

    /**
     * Sets the state whether this plugin is editable
     *
     * @param newState true: edits are allowed
     */
    public final void setEditable(final boolean newState) {
        editableIndicatorProperty().set(newState);
    }

    /**
     * Sets the value of the {@link #labelHorizontalAnchorProperty()}.
     *
     * @param anchor new anchor
     */
    public final void setLabelHorizontalAnchor(final HPos anchor) {
        labelHorizontalAnchorProperty().set(anchor);
    }

    /**
     * Sets the value of the {@link #labelVerticalAnchorProperty()}.
     *
     * @param anchor new anchor
     */
    public final void setLabelVerticalAnchor(final VPos anchor) {
        labelVerticalAnchorProperty().set(anchor);
    }

    public void setStyleClasses(final Node node, final String prefix, final String root) {
        node.getStyleClass().setAll(root, prefix + root, prefix + root + getIndicatorInstanceIndex());
    }

    /**
     * Sets the value of the {@link #textPropertyX()}.
     *
     * @param text the new text. If {@code null}, the label will be hidden.
     */
    public final void setTextX(final String text) {
        textPropertyX().set(text);
    }
    public final void setTextY(final String text) {
        textPropertyY().set(text);
    }

    /**
     * Text to be displayed by the label. If set to {@code null}, the label is not shown.
     *
     * @return text of the indicator's label
     */
    public final StringProperty textPropertyX() {
        return labelX.textProperty();
    }

    public final StringProperty textPropertyY() {
        return labelY.textProperty();
    }
    /**
     * There might be several instances of a given indicator class. If one wants to specify different CSS for each
     * instance - we need a unique class name for each, so whenever the list of plugins changes, this method should
     * update name of it's CSS class.
     */
    public abstract void updateStyleClass();

    // records relative x and y co-ordinates.
    public static class Delta {
        public double x;
        public double y;
    }
}
