package KvetkaSpectrumLib.CustomIndicators;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.AbstractSingleValueIndicator;
import de.gsi.chart.plugins.XValueIndicator;
import de.gsi.chart.plugins.YValueIndicator;
import de.gsi.chart.ui.geometry.Side;
import javafx.event.EventHandler;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;

public class XValueIndicatorCustom extends XValueIndicator {

    public XValueIndicatorCustom(Axis axis, double value) {
        super(axis, value);

    }
    public XValueIndicatorCustom(Axis axis, double value, String text) {
        super(axis, value, text);
    }

    private Color cl = Color.LIGHTGRAY;
    public void setColor(Color cl)
    {
        this.cl = cl;
    }

    @Override
    protected void handleDragMouseEvent(final MouseEvent mouseEvent) {}

    @Override
    public void layoutChildren() {
        if (getChart() == null) {
            return;
        }

        final Bounds plotAreaBounds = getChart().getCanvas().getBoundsInLocal();
        final double minX = plotAreaBounds.getMinX();
        final double maxX = plotAreaBounds.getMaxX();
        final double minY = plotAreaBounds.getMinY();
        final double maxY = plotAreaBounds.getMaxY();
        final double xPos = minX + getAxis().getDisplayPosition(getValue());
        final double axisPos;
        if (getAxis().getSide().equals(Side.BOTTOM)) {
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxis().getCanvas().localToScene(0, 0)).getY() + 6;
        } else {
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxis().getCanvas().localToScene(0, getAxis().getHeight())).getY() - 6;
        }
        final double xPosGlobal = getChart().getPlotForeground().sceneToLocal(getChart().getCanvas().localToScene(xPos, 0)).getX();

        if (xPos < minX || xPos > maxX) {
            getChartChildren().clear();
        } else {
            layoutLine(xPos, minY, xPos, maxY);
            
            this.pickLine.setStroke(cl);
            this.pickLine.setStrokeType(StrokeType.CENTERED);
            this.pickLine.setStrokeWidth(1.5);
            this.line.setStroke(cl);
            this.line.setStrokeType(StrokeType.CENTERED);
            this.line.setStrokeWidth(1.5);
        }


    }
}
