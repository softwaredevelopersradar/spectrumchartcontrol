package KvetkaSpectrumLib.CustomIndicators;


import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.ValueIndicator;
import de.gsi.dataset.event.EventListener;
import de.gsi.dataset.event.EventSource;
import de.gsi.dataset.event.UpdateEvent;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class AbstractDoubleValueIndicatorCustom extends AbstractValueIndicatorCustom
       implements EventSource, ValueIndicator {

   protected static final int DEFAULT_PICKING_DISTANCE = 30;
   protected static final double MIDDLE_POSITION = 0.5;
   protected static final String STYLE_CLASS_LABEL = "value-indicator-label";
   protected static final String STYLE_CLASS_MARKER = "value-indicator-marker";
   protected static double triangleHalfWidth = 5.0;
   private final transient AtomicBoolean autoNotification = new AtomicBoolean(true);
   private final transient List<EventListener> updateListeners = Collections.synchronizedList(new LinkedList<>());
   private boolean autoRemove = false;

   protected final Line pickLineX = new Line();
   protected final Line pickLineY = new Line();
   protected final Polygon triangleY = new Polygon();
   protected final Polygon triangleX = new Polygon();

   private final DoubleProperty pickingDistance = new SimpleDoubleProperty(this, "pickingDistance",
           DEFAULT_PICKING_DISTANCE) {
       @Override
       protected void invalidated() {
           if (get() <= 0) {
               throw new IllegalArgumentException("The " + getName() + " must be a positive value");
           }
       }
   };


   private final DoubleProperty value = new SimpleDoubleProperty(this, "value") {
       @Override
       protected void invalidated() {
           layoutChildren();
       }
   };
   private final DoubleProperty valueX = new SimpleDoubleProperty(this, "valueX") {
       @Override
       protected void invalidated() {
           layoutChildren();
       }
   };
   private final DoubleProperty valueY = new SimpleDoubleProperty(this, "valueY") {
       @Override
       protected void invalidated() {
           layoutChildren();
       }
   };

   private final DoubleProperty labelPositionX = new SimpleDoubleProperty(this, "labelPosition", 0.5) {
       @Override
       protected void invalidated() {
           if (get() < 0 || get() > 1) {
               throw new IllegalArgumentException("labelPosition must be in rage [0,1]");
           }
           layoutChildren();
       }
   };

   private final DoubleProperty labelPositionY = new SimpleDoubleProperty(this, "labelPosition", 0.5) {
       @Override
       protected void invalidated() {
           if (get() < 0 || get() > 1) {
               throw new IllegalArgumentException("labelPosition must be in rage [0,1]");
           }
           layoutChildren();
       }
   };


   protected AbstractDoubleValueIndicatorCustom(Axis axisX, Axis axisY, final double valueX,final double valueY, final String textX, final String textY) {
       super(axisX,axisY, textX, textY);
       setValueX(valueX);
       setValueY(valueY);

       initLines();
       initTriangle();

       chartProperty().addListener((p, o, n) -> {
           if (o != null) {
               o.getPlotForeground().getChildren().remove(triangleX);
               o.getPlotForeground().getChildren().remove(triangleY);
           }
       });

       getChartChildren().addAll(labelX,labelY);

       this.valueX.addListener(
               (ch, o, n) -> invokeListener(new UpdateEvent(this, "valueX changed to " + n + " for axis " + axisX)));
       this.valueY.addListener(
               (ch, o, n) -> invokeListener(new UpdateEvent(this, "valueX changed to " + n + " for axis " + axisY)));
   }

   @Override
   public AtomicBoolean autoNotification() {
       return autoNotification;
   }

   public final double getLabelPositionX() {
       return labelPositionXProperty().get();
   }
   public final double getLabelPositionY() {
       return labelPositionYProperty().get();
   }

   public final double getPickingDistance() {
       return pickingDistanceProperty().get();
   }

   @Override
   public Axis getAxis() {
       return null;
   }

   @Override
   public final double getValue() {
       return valueProperty().get();
   }

   @Override
   public void setText(String text) {

   }

   public final double getValueX() {
       return valuePropertyX().get();
   }
   public final double getValueY() {
       return valuePropertyY().get();
   }

   @Override
   public void setValue(double newValue) {

   }

   private void initLines() {
       // mouse transparent if not editable
       pickLineX.setPickOnBounds(true);
       pickLineX.setStrokeWidth(3);
       pickLineX.setStroke(Color.valueOf("#e5e5e5"));
       pickLineX.mouseTransparentProperty().bind(editableIndicatorProperty().not());
       pickLineX.setOnMousePressed(mouseEvent -> {
           if (mouseEvent.isPrimaryButtonDown()) {
               clickDelta.x = pickLineX.getStartX() - mouseEvent.getX();
               clickDelta.y = pickLineX.getStartY() - mouseEvent.getY();
               mouseEvent.consume();
           }
       });

       pickLineY.setPickOnBounds(true);
       pickLineY.setStrokeWidth(3);
       pickLineY.setStroke(Color.valueOf("#e5e5e5"));
       pickLineY.mouseTransparentProperty().bind(editableIndicatorProperty().not());
       pickLineY.setOnMousePressed(mouseEvent -> {
           if (mouseEvent.isPrimaryButtonDown()) {
               clickDelta.x = pickLineX.getStartX() - mouseEvent.getX();
               clickDelta.y = pickLineX.getStartY() - mouseEvent.getY();
               mouseEvent.consume();
           }
       });
   }

   private void initTriangle() {
       triangleX.visibleProperty().bind(editableIndicatorProperty());
       triangleX.mouseTransparentProperty().bind(editableIndicatorProperty().not());
       triangleX.setPickOnBounds(true);
       triangleX.setOpacity(0.7);
       final double a = AbstractDoubleValueIndicatorCustom.triangleHalfWidth;
       triangleX.getPoints().setAll(-a, -a, -a, +a, +a, +a, +a, -a);
       triangleX.setOnMousePressed(mouseEvent -> {
           clickDelta.x = triangleX.getLayoutX() - mouseEvent.getX();
           clickDelta.y = triangleX.getLayoutY() - mouseEvent.getY();
           mouseEvent.consume();
       });


       triangleY.visibleProperty().bind(editableIndicatorProperty());
       triangleY.mouseTransparentProperty().bind(editableIndicatorProperty().not());
       triangleY.setPickOnBounds(true);
       triangleY.setOpacity(0.7);
       final double a1 = AbstractDoubleValueIndicatorCustom.triangleHalfWidth;
       triangleY.getPoints().setAll(-a1, -a1, -a1, +a1, +a1, +a1, +a1, -a1);
       triangleY.setOnMousePressed(mouseEvent -> {
           clickDelta.x = triangleY.getLayoutX() - mouseEvent.getX();
           clickDelta.y = triangleY.getLayoutY() - mouseEvent.getY();
           mouseEvent.consume();
       });
   }

   public boolean isAutoRemove() {
       return autoRemove;
   }


   public final DoubleProperty labelPositionXProperty() {
       return labelPositionX;
   }
   public final DoubleProperty labelPositionYProperty() {
       return labelPositionY;
   }


   protected void layoutLineX(final double startX, final double startY, final double endX, final double endY) {
       pickLineX.setStartX(startX);
       pickLineX.setStartY(startY);
       pickLineX.setEndX(endX);
       pickLineX.setEndY(endY);
       pickLineX.setStrokeWidth(2);

       addChildNodeIfNotPresent(pickLineX);
   }

   protected void layoutLineY(final double startX, final double startY, final double endX, final double endY) {
       pickLineY.setStartX(startX);
       pickLineY.setStartY(startY);
       pickLineY.setEndX(endX);
       pickLineY.setEndY(endY);
       pickLineY.setStrokeWidth(2);

       addChildNodeIfNotPresent(pickLineY);
   }

   protected void layoutMarkerX(final double startX, final double startY, final double endX, final double endY) {
       if (!triangleX.isVisible()) {
           return;
       }
       triangleX.setTranslateX(startX);
       triangleX.setTranslateY(startY);
       triangleX.toFront();
       if (!getChart().getPlotForeground().getChildren().contains(triangleX)) {
           getChart().getPlotForeground().getChildren().add(triangleX);
       }
   }

   protected void layoutMarkerY(final double startX, final double startY, final double endX, final double endY) {
       if (!triangleY.isVisible()) {
           return;
       }

       triangleY.setTranslateX(startX);
       triangleY.setTranslateY(startY);
       triangleY.toFront();
       if (!getChart().getPlotForeground().getChildren().contains(triangleY)) {
           getChart().getPlotForeground().getChildren().add(triangleY);
       }
   }

   public final DoubleProperty pickingDistanceProperty() {
       return pickingDistance;
   }

   public void setAutoRemove(boolean autoRemove) {
       this.autoRemove = autoRemove;
   }

   public final void setLabelPositionX(final double value) {
       labelPositionXProperty().set(value);
   }
   public final void setLabelPositionY(final double value) {
       labelPositionYProperty().set(value);
   }


   public final void setPickingDistance(final double distance) {
       pickingDistanceProperty().set(distance);
   }

   public final void setValueX(final double newValue) {
       valuePropertyX().set(newValue);
   }
   public final void setValueY(final double newValue) {
       valuePropertyY().set(newValue);
   }

   @Override
   public List<EventListener> updateEventListener() {
       return updateListeners;
   }

   @Override
   public void updateStyleClass() {
   }

   @Override
   public final DoubleProperty valueProperty() {
       return value;
   }

   public final DoubleProperty valuePropertyX() {
       return valueX;
   }

   public final DoubleProperty valuePropertyY() {
       return valueY;
   }
}