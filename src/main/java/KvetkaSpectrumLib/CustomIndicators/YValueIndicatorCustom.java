package KvetkaSpectrumLib.CustomIndicators;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.YValueIndicator;
import de.gsi.chart.ui.geometry.Side;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;

public class YValueIndicatorCustom extends YValueIndicator {


   public YValueIndicatorCustom(Axis axis, double value, String text) {
       super(axis, value, text);
   }

   private Color cl = Color.GREEN;
   public void setColor(Color cl)
   {
       this.cl = cl;
   }


   @Override
   public void layoutChildren() {
       if (this.getChart() != null) {
           Bounds plotAreaBounds = this.getChart().getCanvas().getBoundsInLocal();
           double minX = plotAreaBounds.getMinX();
           double maxX = plotAreaBounds.getMaxX();
           double minY = plotAreaBounds.getMinY();
           double maxY = plotAreaBounds.getMaxY();
           double yPos = minY + this.getAxis().getDisplayPosition(this.getValue());
           double axisPos;
           if (this.getAxis().getSide().equals(Side.RIGHT)) {
               axisPos = this.getChart().getPlotForeground().sceneToLocal(this.getAxis().getCanvas().localToScene(0.0D, 0.0D)).getX() + 2.0D;
               this.triangle.getPoints().setAll(0.0D, 0.0D, 8.0D, 8.0D, 8.0D, -8.0D);
           } else {
               axisPos = this.getChart().getPlotForeground().sceneToLocal(this.getAxis().getCanvas().localToScene(this.getAxis().getWidth(), 0.0D)).getX() - 2.0D;
               this.triangle.getPoints().setAll(0.0D, 0.0D, -8.0D, 8.0D, -8.0D, -8.0D);
           }

           double yPosGlobal = this.getChart().getPlotForeground().sceneToLocal(this.getChart().getCanvas().localToScene(0.0D, yPos)).getY();
           if (yPos >= minY && yPos <= maxY) {
               this.layoutMarker(axisPos, yPosGlobal, minX, yPos);
               this.layoutLabel(new BoundingBox(minX, yPos, maxX - minX, 0.0D), this.getLabelPosition(), 0.5D);
               this.layoutLine(minX, yPos, maxX, yPos);
               this.line.setStroke(cl);
               this.line.setStrokeType(StrokeType.CENTERED);
               this.line.setStrokeWidth(2.5);
           } else {
               this.getChartChildren().clear();
           }

       }



   }
}
