package KvetkaSpectrumLib.Drawing;

import de.gsi.chart.axes.Axis;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Stick extends Line {

    private double frequency;
    private double y;
    private ObservableList<Axis> axis;
    public Stick(ObservableList<Axis> axis, double frequency, double y)
    {
        this.frequency = frequency;
        this.axis = axis;
        this.y = y;
        layout(axis.get(0).getDisplayPosition(frequency), axis.get(1).getDisplayPosition(y));
    }

    private void layout(double x, double y)
    {
        setStartX(x);
        setEndX(x);
        setStartY(y);
        setEndY(y - 50);
        setStroke(Color.RED);
        setStrokeWidth(3);
    }

    public void UpdateLayout()
    {
        layout(axis.get(0).getDisplayPosition(frequency), axis.get(1).getDisplayPosition(y));
    }

    public double getXScreen()
    {
        return getStartX();
    }

    public double getYScreen()
    {
        return getStartY();
    }

    public double getFrequency()
    {
        return frequency;
    }
}
