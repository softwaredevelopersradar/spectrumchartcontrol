package KvetkaSpectrumLib.Drawing;

import KvetkaSpectrumLib.CustomRenders.ArrowMarkerRenderCustom;
import KvetkaSpectrumLib.Exceptions.NotFoundHeadException;
import de.gsi.chart.XYChart;
import de.gsi.chart.plugins.ChartPlugin;
import de.gsi.dataset.spi.DoubleDataSet;
import javafx.scene.paint.Color;

import java.util.Arrays;

public class DrawArea {
    private static int ARROWS_MAX_AMOUNT = 35;
    private final DoubleDataSet arrowsBuffer = new DoubleDataSet("arrows", ARROWS_MAX_AMOUNT);
    private final ArrowMarkerRenderCustom arrowMarkerRenderer = new ArrowMarkerRenderCustom();
    private final double Epsilon = 0.5;
    private XYChart chart;


    public DrawArea(XYChart chart)
    {
        this.chart = chart;
        arrowsBuffer.setStyle("strokeColor=" + Color.RED+";" +"strokeWidth=1.8;");
        InitArrowsSetRenderer();
    }



    public void addArrow(double frequency)
    {
        if(frequency < chart.getXAxis().getMin() || frequency > chart.getXAxis().getMax())
            return;

        removeArrow(frequency);

        double y = chart.getAllDatasets().get(0).getValue(1,frequency);
        arrowsBuffer.add(frequency, y);
    }

    public void removeArrow(double frequency)
    {
        for(int i = 0; i < arrowsBuffer.getDataCount(); i++)
        {
            if(Math.abs(arrowsBuffer.getX(i) - frequency) <= Epsilon)
            {
                arrowsBuffer.remove(i);
                break;
            }
        }
    }

    public void setArrows(double[] frequencies)
    {
        double[] x = new double[frequencies.length];
        double[] y = new double[frequencies.length];
        for(int i = 0; i < frequencies.length;i++)
        {
            x[i] = frequencies[i];

            double y1 = chart.getAllDatasets().get(0).getValue(1,frequencies[i] - Epsilon);
            double y2 = chart.getAllDatasets().get(0).getValue(1,frequencies[i]);
            double y3 = chart.getAllDatasets().get(0).getValue(1,frequencies[i] + Epsilon);

            y[i] = Math.max(Math.max(y1, y2), y3);

        }
        arrowsBuffer.set(x,y);
    }

    public void clearArrows()
    {
        arrowsBuffer.clearData();
    }

    public void IsSignalJamming(double frequency, boolean isSignalJamming)
    {
        for(int i = 0; i < arrowsBuffer.getDataCount(); i++)
        {
            if(Math.abs(arrowsBuffer.getX(i) - frequency) <= Epsilon)
                if(isSignalJamming)
                    arrowsBuffer.addDataStyle(i,"strokeColor=" + Color.rgb(255,0,0)+";");
                else
                    arrowsBuffer.addDataStyle(i,"strokeColor=" + Color.rgb(255,255,255)+";");
        }
    }

    public void IsSignalJamming(int index, boolean isSignalJamming)
    {
        if(isSignalJamming)
            arrowsBuffer.addDataStyle(index,"strokeColor=" + Color.rgb(255,0,0)+";");
        else
            arrowsBuffer.addDataStyle(index,"strokeColor=" + Color.rgb(255,255,255)+";");
    }







    public void addRangeHead(double startFrequency, double endFrequency)
    {
        try {
            getRangeHead(startFrequency, endFrequency);
        } catch (NotFoundHeadException e) {
            RangeHead rangeHead = new RangeHead(chart,startFrequency, endFrequency);
            chart.getPlugins().add(rangeHead);
        }
    }

    public void removeRangeHead(double startFrequency, double endFrequency)
    {
        for(ChartPlugin plugin : chart.getPlugins())
        {
            if(plugin.getClass() == RangeHead.class)
            {
                RangeHead rangeHead = (RangeHead)plugin;
                if(Math.abs(rangeHead.getStartFrequency() - startFrequency) <= Epsilon && Math.abs(rangeHead.getEndFrequency() - endFrequency) <= Epsilon)
                {
                    chart.getPlugins().remove(rangeHead);
                    return;
                }
            }
        }
    }

    public RangeHead getRangeHead(double startFrequency, double endFrequency) throws NotFoundHeadException {

        for(ChartPlugin plugin : chart.getPlugins())
        {
            if(plugin.getClass() == RangeHead.class)
            {
                RangeHead rangeHead = (RangeHead)plugin;
                if(Math.abs(rangeHead.getStartFrequency() - startFrequency) <= 0.5 && Math.abs(rangeHead.getEndFrequency() - endFrequency) <= 0.5)
                    return rangeHead;
            }
        }
        throw new NotFoundHeadException("Range Head not found ", startFrequency, endFrequency);
    }




    public void setSticks(double[] sticks, int index)
    {
        removeRangeHead(index);

        double minimumValue = Arrays.stream(sticks).min().getAsDouble();
        double MaxmumValue =  Arrays.stream(sticks).max().getAsDouble();

        RangeHead rangeHead = new RangeHead(chart,minimumValue, MaxmumValue, index);

        chart.getPlugins().add(rangeHead);
        rangeHead.setSticks(sticks);

    }

    public void removeSticks(int index)
    {
        removeRangeHead(index);
    }

    public RangeHead getRangeHead(int index) throws NotFoundHeadException {
        for(ChartPlugin plugin : chart.getPlugins())
        {
            if(plugin.getClass() == RangeHead.class)
            {
                RangeHead rangeHead = (RangeHead)plugin;
                if(rangeHead.getIndex() == index)
                {
                    return rangeHead;
                }
            }
        }
        throw new NotFoundHeadException("Range Head not found, index = " + index, 0,0 );
    }

    public void removeRangeHead(int index) {
        for(ChartPlugin plugin : chart.getPlugins())
        {
            if(plugin.getClass() == RangeHead.class)
            {
                RangeHead rangeHead = (RangeHead)plugin;
                if(rangeHead.getIndex() == index)
                {
                    chart.getPlugins().remove(rangeHead);
                    return;
                }
            }
        }
    }

    private void InitArrowsSetRenderer()
    {
        arrowMarkerRenderer.getAxes().addAll(chart.getYAxis());
        arrowMarkerRenderer.getDatasets().add(arrowsBuffer);
        arrowMarkerRenderer.enableVerticalMarker(true);
        chart.getRenderers().add(arrowMarkerRenderer);
    }
}
