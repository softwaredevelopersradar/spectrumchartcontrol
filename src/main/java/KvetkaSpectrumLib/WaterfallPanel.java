package KvetkaSpectrumLib;

import KvetkaSpectrumLib.Waterfall.WaterfallChart;
import de.gsi.chart.axes.Axis;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.*;

import java.io.IOException;

public class WaterfallPanel extends AnchorPane {

    @FXML
    private WaterfallChart waterfallChart;

    private final String axisYName = "Уровень";
    private final String axisYUnit = "[Дб]";
    private Canvas canvas;
    private Slider slider;
    private boolean isMovedByButton = true;
    private ChangeListener SliderValueListener;
    private final double diffSliderValue = 1.0/600.0;


    public WaterfallPanel() throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/WaterfallPanel.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        SliderValueListener = new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                //if(t1.doubleValue() == 0.0 || number.doubleValue() == 0.0) return;
                double diff = t1.doubleValue()-number.doubleValue();
                boolean tr = waterfallChart.setGradientDependFromCoefficient_RainBow(diff);
                if(tr)
                    RedrawZRectangle(canvas);
            }
        };



        InitializeCanvas();


    }

    private void InitializeCanvas()
    {
        DefaultNumericAxis zAxis = waterfallChart.getzAxis();
        zAxis.setMaxWidth(40);
        zAxis.setViewOrder(1);

        canvas = new Canvas(40,200);
        canvas.setViewOrder(2);
        canvas.getGraphicsContext2D().setLineWidth(0.0);

        Label labelName = new Label(axisYName);
        Label labelUnit = new Label(axisYUnit);
        labelName.setStyle("-fx-font-size: 10;");
        labelUnit.setStyle("-fx-font-size: 12;");
        labelUnit.setTranslateX(10);

        slider = new Slider();
        slider.setOrientation(Orientation.VERTICAL);
        slider.setBlockIncrement(diffSliderValue);
        slider.setShowTickLabels(false);
        slider.setShowTickMarks(false);
        slider.setViewOrder(0);
        slider.setMax(1);
        slider.setMin(0);
        slider.setTranslateY(-3);
        slider.valueProperty().addListener(SliderValueListener);

        Pane pane = new Pane();
        pane.setMaxWidth(40);
        pane.setMinWidth(40);
        pane.setTranslateX(10);
        pane.setTranslateY(15);
        pane.getChildren().add(canvas);
        pane.getChildren().add(zAxis);
        pane.getChildren().add(labelName);
        pane.getChildren().add(labelUnit);
        this.getChildren().add(pane);
        pane.getChildren().add(slider);

        waterfallChart.heightProperty().addListener(event -> {
            double height = waterfallChart.getHeight();
            pane.setPrefHeight(height);
            pane.setMinHeight(height);
            pane.setMaxHeight(height);
        });

        canvas.heightProperty().bind(pane.prefHeightProperty());
        canvas.heightProperty().addListener(event ->
        {
            double height = pane.getPrefHeight() - 53;

            zAxis.setMinHeight(height);
            zAxis.setMaxHeight(height);
            labelName.setTranslateY(height + 5);
            labelUnit.setTranslateY(height + 17);

            RedrawZRectangle(canvas);
        });
    }

    private void RedrawZRectangle(Canvas canvas)
    {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        double gradientHeight = canvas.getHeight() - 50;
        LinearGradient waterfallGradient = waterfallChart.getColorGradient();

        graphicsContext.beginPath();
        graphicsContext.clearRect(0,0,40,canvas.getHeight());
        graphicsContext.setFill(waterfallGradient);
        graphicsContext.fillRect(0,0,40,gradientHeight);
        graphicsContext.closePath();
        graphicsContext.restore();
        graphicsContext.save();

        slider.valueProperty().removeListener(SliderValueListener);
        slider.setMinHeight(gradientHeight);
        slider.setPrefHeight(gradientHeight);
        slider.setValue(waterfallChart.getGradientCoefficient());
        slider.valueProperty().addListener(SliderValueListener);
    }

    public Axis getZAxis()
    {
        return waterfallChart.getzAxis();
    }

    public WaterfallChart getWaterfallChart()
    {
        return waterfallChart;
    }

    public Axis getXAxis()
    {
        return waterfallChart.getXAxis();
    }

    public Axis getYAxis()
    {
        return waterfallChart.getYAxis();
    }



    public void setDataSet(double[] bytes)
    {
        waterfallChart.setDataSet(bytes);
    }

    public void clearDataSet()
    {
        waterfallChart.clearDataSet();
    }

    public void setZAxis(double max, boolean isExtend)
    {
        waterfallChart.setZAxis(max, isExtend);
        RedrawZRectangle(canvas);
    }


    public void setCoeffGradient(double coeff)
    {
        double newSliderValue = diffSliderValue * (coeff < 0 ? -1.0 : 1.0) + slider.getValue();
        slider.setValue(newSliderValue);
//        waterfallChart.setWaterfallGradient_ByScrolling(coeff);
//        RedrawZRectangle(canvas);
    }

    public void setDataSetSize(int size)
    {
        waterfallChart.DataSetInit(size);
    }
}
