package KvetkaSpectrumLib;

import KvetkaModels.AnalogReconFWSModel;
import KvetkaSpectrumLib.Exceptions.NotFoundHeadException;
import KvetkaSpectrumLib.Interfaces.*;
import KvetkaSpectrumLib.Models.SignalOnTrackingModel;
import de.gsi.dataset.utils.ByteArrayCache;
import de.gsi.dataset.utils.DoubleArrayCache;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main extends Application implements ICallBackSound, ICallBackSignalEvents, ICallBackCommonChartEvents, ICallBackAthenuators, ICallBackFilters, ICallBackTimeSlider {
byte[] data;
    int deley = 200;
    Pane_Spectr paneSpectr;
    int j = 0;
    int k = 0;

    @Override
    public void start(Stage primaryStage)  throws Exception {
        paneSpectr = new Pane_Spectr();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(paneSpectr, 1200, 800));
        primaryStage.setResizable(true);
        primaryStage.show();
        paneSpectr.registerCallBackCommonChartEvents(this);
        paneSpectr.registerCallBackSignalEvents(this);
        paneSpectr.registerCallBackAthenuators(this);
        paneSpectr.registerCallBackFilters(this);
        paneSpectr.registerCallBackSound(this);
        paneSpectr.registerCallBackTimeSlider(this);
        paneSpectr.setSelectEveryValueCount(1);
        paneSpectr.getAttenuatorsControl().setItemInSignalState(true);
        paneSpectr.getFiltersControl().setItemInSignalState(true);
        paneSpectr.setMinTimeSlider(-300);


        ArrayList<String> modulation = new ArrayList<String>();
        modulation.add("Am");
        modulation.add("Fm");
        //paneSpectr.getSmallChart().addItemsModulation(modulation);

        ArrayList<Double> line = new ArrayList<Double>();
        line.add(5000d);
        line.add(15000d);
        //paneSpectr.getSmallChart().addItemsLine(line);


        System.out.println("init finish");


        List<Integer> list = new ArrayList<Integer>();
        list.add(0);
        list.add(10);
        list.add(20);
        list.add(40);
        List<Double> listd = new ArrayList<>();
        listd.add(0.0);
        listd.add(10.5);
        listd.add(20.6);
        listd.add(40.5);

        paneSpectr.getAttenuatorsControl().AddItemsList(list);
        paneSpectr.getAttenuatorsControl().AddOneItem(1,"5");

        paneSpectr.getFiltersControl().AddItemsList(listd);
        paneSpectr.getFiltersControl().AddOneItem(1,"5.4");



        data = new byte[3];
        data[0] = 10;
        data[1] = 30;
        data[2] = 50;

        // paneSpectr.UpdateSpectr(data, 11000, 13000);
        // paneSpectr.AddSignalOnTracking(17000);

        final int size = 285000;
        paneSpectr.InitSizeWaterfallDataSet(285000);

        LoadTrackingSignalFromJSON();

        Runnable task = new Runnable() {


            boolean i = true;
            @Override
            public void run() {
                //paneSpectr.startWaterfallTimer();
                //DisplayFromFile();
                DisplayFullRandomGenerated(size);
                //UpdateSubSpectrum();
            }
        };


        Thread thread = new Thread(task);
        thread.start();

    }

    private void UpdateSubSpectrum()
    {
        byte[] data1 = new byte[2000];
        for(int i = 0; i < data1.length; i++)
        {
            data1[i] = (byte) ThreadLocalRandom.current().nextInt(90, 110);
        }

        paneSpectr.UpdateSubSpectrum(data1.clone(),this.startFreq ,this.endFreq );

    }

    private void DisplayPartialRandomGenerated(int size)
    {
        while (true){
            long startTime = System.currentTimeMillis();
            DoubleArrayCache.getInstance().clear();
            ByteArrayCache.getInstance().clear();
            deley = 32;
            try {
                Thread.sleep(deley);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }



            Platform.runLater(() -> {
                data = new byte[size];
                for(int i = 0; i < data.length; i++)
                {
                    data[i] = (byte) ThreadLocalRandom.current().nextInt(90, 110);
                }

                double v1 = ThreadLocalRandom.current().nextInt(20,90);


                for(int i = 0; i < 90; i++)
                {
                    double v = Math.sin(Math.toRadians(i * 2)) * v1;
                    //byte tt = (byte)(90 - ThreadLocalRandom.current().nextInt(13, 18) * i);

                    data[data.length/2 + i] = (byte) ThreadLocalRandom.current().nextInt(80 - (int) v,100 - (int) v);
                }

                paneSpectr.UpdateSpectr(data.clone(), 1500 + 500 * j, 2000 + 500 * j);
                if(j >= 56)
                {
                    j=0;

                }
                else j++;



                System.gc();
            });
        }
    }

    int ff = 0;
    int parts = 4;
    private void DisplaySomeParts(int size)
    {
        while (true){
            long startTime = System.currentTimeMillis();
            DoubleArrayCache.getInstance().clear();
            ByteArrayCache.getInstance().clear();
            deley = 16;
            try {
                Thread.sleep(deley);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(ff == 120)
                parts = 3;
            ff++;


            Platform.runLater(() -> {
                byte[][] dt = new byte[parts][size];
                for(int k = 0; k < parts; k++)
                {
                    dt[k] = new byte[size];
                }
                double[] start = new double[parts];
                double[] end = new double[parts];

                for(int k = 0; k < start.length; k++)
                {
                    data = new byte[size];
                    for(int i = 0; i < data.length; i++)
                    {
                        data[i] = (byte) ThreadLocalRandom.current().nextInt(90, 110);
                    }

                    double v1 = ThreadLocalRandom.current().nextInt(20,90);


                    for(int i = 0; i < 90; i++)
                    {
                        double v = Math.sin(Math.toRadians(i * 2)) * v1;
                        //byte tt = (byte)(90 - ThreadLocalRandom.current().nextInt(13, 18) * i);

                        data[data.length/2 + i] = (byte) ThreadLocalRandom.current().nextInt(80 - (int) v,100 - (int) v);
                    }

                    dt[k] = data;
                    double st = 2000 + 3000*k;
                    start[k] = st;
                    end[k] = st + 1000;
                }

                paneSpectr.UpdateSpectr(dt, start, end);

            });
        }
    }


    private void DisplayFullRandomGenerated(int size)
    {
        var ref = new Object() {
            boolean bbb = true;
        };


        while (true){
            long startTime = System.currentTimeMillis();

            deley = 64;
            try {
                Thread.sleep(deley);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            data = new byte[size];
            for(int i = 0; i < data.length; i++)
            {
                data[i] = (byte) ThreadLocalRandom.current().nextInt(90, 110);
            }

            for(int j=1;j<10;j++)
            {
                double v1 = ThreadLocalRandom.current().nextInt(50,60);

                for(int i = 0; i < 15; i++)
                {
                    double v = Math.sin(Math.toRadians(i * 12)) * v1;
                    //byte tt = (byte)(90 - ThreadLocalRandom.current().nextInt(13, 18) * i);

                    data[(data.length - (int)Math.round(size/10)*j) + i] = (byte) ThreadLocalRandom.current().nextInt((95 - (int) v),(100 - (int) v));

                }

                data[(data.length - (int)Math.round(size/10)*5) + 7] = -36;
            }


            Platform.runLater(() -> {
                if(data.length > 80527)
                    if(data[80527] == 0 && data[80526] == 0 && data[80528] == 0)
                        data.clone();
                paneSpectr.UpdateSpectr(data, 1500, 30000);
                double[] arrows = new double[10];

                for(int i = 0 ; i < arrows.length; i++)
                {
                    double d = ThreadLocalRandom.current().nextDouble(0, 300);
                    arrows[i] = 2500 + 2000*i + d;
                }
                paneSpectr.getDrawingArea().setArrows(arrows);
                paneSpectr.getDrawingArea().IsSignalJamming(3,false);

                double[] sticks = new double[20];
                double b = 0;
                for(int i = 0 ; i < sticks.length; i++)
                {
                    double d = ThreadLocalRandom.current().nextDouble(0, 200);
                    b += 100 + d;
                    sticks[i] = 4000 + b;
                }

                paneSpectr.getDrawingArea().addRangeHead(3000,10000);
                try {
                    paneSpectr.getDrawingArea().getRangeHead(3000,10000).setSticks(sticks);
                } catch (NotFoundHeadException e) {
                    e.printStackTrace();
                }

                if(ref.bbb)
                {
                    //paneSpectr.getSmallChart().setFrequencyInSoundControl(15000);
                    ref.bbb =false;
                }

                UpdateSubSpectrum();
            });

            if(j >= 30)
            {

                data[(data.length - (int)Math.round(size/10)*5) + 7] = -36;
            }
            if (j < 30)
            {
                data[(data.length - (int)Math.round(size/10)*5) + 7] = -25;
            }
            if(j >= 60)
            {
                j=0;
            }
            else j++;


            System.gc();
        }
    }


    private void DisplayFromFile()
    {
        for(int i = 1; i < 38; i++){
            if(i == 37)
                i=1;
            DoubleArrayCache.getInstance().clear();
            ByteArrayCache.getInstance().clear();

            data = OpenFiles(i);


            Platform.runLater(() -> {

                paneSpectr.UpdateSpectr(data.clone(), 1500 , 30000);

            });
        }
    }

    private byte[] OpenFiles(int index)
    {
        byte[] list = new byte[285000];
        try {
            // create a reader
            FileInputStream fis = new FileInputStream("D:\\Yagello_All_Projects\\Main_Projects\\My_Projects\\KvetkaSpecrumLib\\src\\main\\resources\\ByteFile\\_0powers-" + index + ".dat");

            // read one byte at a time
            int ch;
            int i = 0;
            while ((ch = fis.read()) != -1) {
                if(ch != 0)
                {
                    list[i] = (byte)ch;
                    i++;
                }
            }

            System.out.println(index);
            // close the reader
            fis.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) {
        launch(args);
    }

    String path = "SignalOnTracking.json";
    @Override
    public void signalAdded(double frequency,  AnalogReconFWSModel analogReconFWSModel){
        System.out.println("Added signal with frequency :" + frequency +
                "\nId: " + analogReconFWSModel.getId() +
                "\nDeltaF: " + analogReconFWSModel.getBand() +
                "\nLevel: " + analogReconFWSModel.getLevel() +
                "\n\n\n");

        SaveTrackingSignalToJSON();
    }

    private void SaveTrackingSignalToJSON()
    {
        JSONArray jsn = new JSONArray();
        var model = paneSpectr.GetTrackingSignalsModel();
        for(int i=0; i< model.size(); i++)
        {
            jsn.put(model.get(i).toMap());
        }

        try (PrintWriter out = new PrintWriter(new FileWriter(path))) {
            out.write(jsn.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadTrackingSignalFromJSON()
    {
        Path filePath = Path.of(path);
        String jsn = "";
        ArrayList<SignalOnTrackingModel> signalOnTrackingModels = new ArrayList<>();
        try {
            jsn = Files.readString(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = new JSONArray(jsn);
            for(int i=0; i < jsonArray.length();i++){
                JSONObject jo = jsonArray.getJSONObject(i);
                SignalOnTrackingModel newModel = new SignalOnTrackingModel(jo.getDouble("frequency"), jo.getDouble("range"));
                signalOnTrackingModels.add(newModel);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        paneSpectr.SetTrackingSignalsModel(signalOnTrackingModels);

    }



    @Override
    public void signalDeleted(double frequency, AnalogReconFWSModel analogReconFWSModel) {
        System.out.println("Deleted signal with frequency :" + frequency +
                "\nId: " + analogReconFWSModel.getId() +
                "\nDeltaF: " + analogReconFWSModel.getBand() +
                "\nLevel: " + analogReconFWSModel.getLevel() +
                "\n\n\n");
        SaveTrackingSignalToJSON();
    }

    @Override
    public void signalBecameUnavailable(double frequency, AnalogReconFWSModel analogReconFWSModel) {
        System.out.println("Unavailable signal with frequency :" + frequency +
                "\nId: " + analogReconFWSModel.getId() +
                "\nDeltaF: " + analogReconFWSModel.getBand() +
                "\nLevel: " + analogReconFWSModel.getLevel() +
                "\n\n\n");
    }

    @Override
    public void signalBecameAvailableAgain(double frequency, AnalogReconFWSModel analogReconFWSModel) {
        System.out.println("Available signal with frequency :" + frequency +
                "\nId: " + analogReconFWSModel.getId() +
                "\nDeltaF: " + analogReconFWSModel.getBand() +
                "\nLevel: " + analogReconFWSModel.getLevel() +
                "\n\n\n");
    }

    double startFreq;
    double endFreq;

    @Override
    public void recuestSubSpectrum(double startFreq, double endFreq) {
        Random random = new Random();
        byte[] data = new byte[100];

        this.startFreq = startFreq;
        this.endFreq = endFreq;
        System.out.println("RECQEST SUB SPECTRUM");
    }

    @Override
    public void thresholdChanged(double threshold) {
        //System.out.println("Threshold changed on: " + threshold);
    }

    @Override
    public void buttonAttenuatorSelected(int buttonText) {
        System.out.println("button with text " + buttonText + " clicked" );
    }

    @Override
    public void buttonFilterSelected(double buttonText) {
        System.out.println("button with text " + buttonText + " clicked" );
    }


    @Override
    public void Playing(AnalogReconFWSModel analogReconFWSMode, boolean isPlaying) {
        if(isPlaying)
        {
            System.out.println("Playing...");
            paneSpectr.getSmallChart().setPlayButtonAsSignal();
        }
        else {
            System.out.println("Stop playing");
            paneSpectr.getSmallChart().setPlayButtonAsNotSignal();
        }
    }

    @Override
    public void SpectrumRequest(int timeInSeconds) {
        System.out.println("request dpectrum from "+ timeInSeconds + " seconds ago");
    }
}
