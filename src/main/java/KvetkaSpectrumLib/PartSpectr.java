package KvetkaSpectrumLib;

import KvetkaSpectrumLib.CustomIndicators.XValueIndicatorCustom;
import KvetkaSpectrumLib.Interfaces.ICallBackCommonChartEvents;
import KvetkaSpectrumLib.Interfaces.ICallBackFrequencyChangedInSubSpectrum;
import KvetkaSpectrumLib.Signal.SignalMath;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Основной класс графика для части спектра
 * @autor Ягелло Александр, инженер-программист из чумного барака
 * @version 1.0
 */
public class PartSpectr extends VBox implements Initializable {

@FXML
private OnlyChart chartForOneSignal;
@FXML
private Label freq;
@FXML
private Label level;
@FXML
private Label deltaF;
@FXML
private ComboBox comboBoxModulation;
@FXML
private ComboBox comboBoxStep;
@FXML
private ComboBox comboBoxLine;
@FXML
private ToggleButton playButton;
@FXML
private Button upFreq;
@FXML
private Button downFreq;
@FXML
private TextField freqTextField;

private int selectedSignalButtonId;

private final String StyleToggleButtonAsSignal = "-fx-background-color:\n" +
        "            linear-gradient( to left, derive(#007038, 35%) 10%, derive(#007038, 10%) 40%, derive(#007038, -8%) 60%, derive(#007038, -30%) 90%);";
private final String StyleToggleButtonAsNotSignal = ".toggle-button {\n" +
        "    -fx-background-color: #292929;\n" +
        "    -fx-border-radius: 3;\n" +
        "    -fx-border-color: grey;\n" +
        "    -fx-padding: 2;\n" +
        "    -fx-text-fill: white;\n" +
        "}\n" +
        "\n" +
        ".toggle-button:selected {\n" +
        "   -fx-background-color:\n" +
        "            linear-gradient( to left, derive(#056794, 35%) 10%, derive(#056794, 10%) 40%, derive(#056794, -8%) 60%, derive(#056794, -30%) 90%);\n" +
        "}";
private XValueIndicatorCustom xValueIndicatorCustom;

private boolean isOutsideAddingFrequency = false;
private ICallBackFrequencyChangedInSubSpectrum iCallBackFrequencyChangedInSubSpectrum;



    public PartSpectr() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/PartSpectr.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        InitComboBoxStep();

        ArrayList<String> modulation = new ArrayList<String>();
        modulation.add("AM");
        modulation.add("FM");
        modulation.add("USB");
        modulation.add("LSB");
        modulation.add("CW");
        addItemsModulation(modulation);

        ArrayList<Double> line = new ArrayList<Double>();
        line.add(5000d);
        line.add(15000d);
        addItemsLine(line);

        chartForOneSignal.setBufferCapacity(100000);
        //chartForOneSignal.disabledZoomer();

        //xValueIndicatorCustom = new XValueIndicatorCustom(chartForOneSignal.getXAxis(), 0);
        //chartForOneSignal.getPlugins().add(xValueIndicatorCustom);
       // double middle = Math.abs((chartForOneSignal.getXAxis().getMin() + chartForOneSignal.getXAxis().getMax()) / 2.0);
        //setMiddleIndicatorValue(middle);

        freqTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
//                if (!newValue.matches("^-?[0-9][0-9,\\.]+$")) {
//                    freqTextField.setText(oldValue);
//                }
//                else {
                    if(isOutsideAddingFrequency)
                    {
                        isOutsideAddingFrequency = false;
                        return;
                    }
                    if(oldValue == "" && newValue == "") return;
                    double oldV = 0;
                    double newV = 0;
                    try {
                        oldV = Double.parseDouble(oldValue);
                        newV = Double.parseDouble(newValue);
                    }catch (Exception ex){}

                    iCallBackFrequencyChangedInSubSpectrum.FrequencyChanged(oldV, newV, selectedSignalButtonId);
                }
//            }
        });
    }

    public void setLevelValue(double value)
    {
        chartForOneSignal.setYValueIndicatorValue(value);
    }

    public void setMin(double value) { chartForOneSignal.getXAxis().setMin(value); }

    public void setMax(double value) { chartForOneSignal.getXAxis().setMax(value); }

    public void registerCallBackCommonChartEvents(ICallBackFrequencyChangedInSubSpectrum iCallBackFrequencyChangedInSubSpectrum)
    {
        this.iCallBackFrequencyChangedInSubSpectrum = iCallBackFrequencyChangedInSubSpectrum;
    }

    public void setFrequencyInSoundControl(double value, int signalButtonId) {
        selectedSignalButtonId = signalButtonId;

        Platform.runLater(() -> {
            freqTextField.setText((Math.round(value * 1000.0) / 1000.0 ) + "");
            freqTextField.positionCaret(freqTextField.getText().length());
        });
        isOutsideAddingFrequency = true;
    }

    public void setRange(double range)
    {
        Platform.runLater(() -> {

            if(range < 4.5)
                comboBoxLine.getSelectionModel().select(0);
            else if(range >=4.5 && range <= 7.0)
                comboBoxLine.getSelectionModel().select(1);
            else comboBoxLine.getSelectionModel().select(2);
        });
    }

    public void setPlayButtonAsSignal()
    {
        playButton.setStyle(StyleToggleButtonAsSignal);
    }
    public void setPlayButtonAsNotSignal()
    {
        playButton.setSelected(false);
        playButton.setStyle(StyleToggleButtonAsNotSignal);
    }

    /**
     * get main chart object
     *
     */
    public OnlyChart getChart() { return chartForOneSignal; }

    public ObjectProperty<EventHandler<ActionEvent>> getEventPlayButton()
    {
        return playButton.onActionProperty();
    }

    public TextField getFrequencyTextBox()
    {
        return freqTextField;
    }

    public ComboBox getComboBoxLine() { return comboBoxLine; }

    public ComboBox getComboBoxModulation() { return comboBoxModulation; }

    public ToggleButton getPlayButton() { return playButton; }


    public double getFrequency()
    {
        String valueStr = freqTextField.getText();
        double value = Double.parseDouble(valueStr);
        return value;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        chartForOneSignal.madeCrossLineControlUnVisible();
        chartForOneSignal.setZoomDepth(1);
        chartForOneSignal.madeThresholdUnmovable();
    }

    public void UpdateSpectr(byte[] newDataSet, double startFreq, double endFreq)
    {
        chartForOneSignal.setMaximumXValue(endFreq);
        chartForOneSignal.setMinimumXValue(startFreq);
        chartForOneSignal.getXAxis().set(startFreq, endFreq);
        chartForOneSignal.getXAxis().forceRedraw();
        double[] data = chartForOneSignal.PlotSubSpectr(startFreq, endFreq, newDataSet);
        //setMiddleIndicatorValue(Math.abs((startFreq + endFreq) / 2.0));
        UpdateLabels(data, startFreq, endFreq, SignalMath.isSignalExist(data,chartForOneSignal.getValue_Porog()));
    }

    public void UpdateLabels(double[] newDataSet, double startFreq, double endFreq, boolean isSignalExist)
    {
        String deltaf;
        String maxLevel;
        String frequency;
        Double deltaFValue;
        Double maxLevelValue;
        Double frequencyValue;


        if(isSignalExist)
        {
            deltaFValue = SignalMath.CalculateDeltaFForSignal(newDataSet.clone(),startFreq,endFreq);
            maxLevelValue = SignalMath.FindMaxLevel(newDataSet.clone());
            frequencyValue = SignalMath.CalculateFrequency(newDataSet.clone(),startFreq,endFreq);
            deltaf = "  ΔF = ".concat(String.valueOf(Math.round(deltaFValue * 100.)/100.)).concat(" кГц");
            maxLevel = "  U = ".concat(String.valueOf((Math.round(maxLevelValue * 10.)/10.))).concat(" дб");
            frequency = "  F = ".concat(String.valueOf(Math.round(frequencyValue * 100.)/100.)).concat(" кГц");
        }
        else {
            maxLevelValue = Math.round(chartForOneSignal.getValue_Porog() * 10.0) / 10.0;
            deltaf = "  ΔF = 0.00 кГц";
            maxLevel = "  U < ".concat(String.valueOf(maxLevelValue).concat(" дб"));
            frequency = "  F = " + (Math.round(((startFreq + endFreq)/2.0) * 10.0)/10.0);
        }

        level.setText(maxLevel);

        freq.setText(frequency);

        deltaF.setText(deltaf);
    }

    public void DefaultsLabels()
    {
        level.setText("  U = 0.0 дб");

        freq.setText("  F = 0.000 кГц");

        deltaF.setText("  ΔF = 0.00 кГц");
    }

    public String getSelectedItemModulation()
    {
        String value = getSelectedItem(comboBoxModulation);
        if(value == null)
            return "";
        return value;
    }

    public Double getSelectedItemLine()
    {
        try{
            Double value = Double.parseDouble(getSelectedItem(comboBoxLine));
            if(value == null)
                return -1d;
            return value;
        }
        catch (Exception ex)
        {
            return -1.0;
        }
    }

    public void addItemsModulation(List<String> list)
    {
        addItemsList(list, comboBoxModulation);
    }

    public void addItemsLine(List<Double> list)
    {
        if(!list.contains(4000.0))
            list.add(0,4000.0);

        addItemsList(list, comboBoxLine);
        comboBoxLine.getSelectionModel().select(0);
    }

//    private void setMiddleIndicatorValue(double value)
//    {
//        xValueIndicatorCustom.setValue(value);
//        xValueIndicatorCustom.layoutChildren();
//    }

    private String getSelectedItem(ComboBox comboBox)
    {
        String str = (String) comboBox.getValue();
        if(str == null)
           return null;

        if (!str.equals("null"))
            return str;
        else
        {
            if(comboBox.getItems().size() == 0)
                return null;
            else  return (String) comboBox.getItems().get(0);
        }
    }

    private <T> void addItemsList(List<T> list, ComboBox comboBox)
    {
        if(list != null){
            comboBox.getItems().clear();
            List<String> newList = new ArrayList<>();

            for(T item : list)
                newList.add(item.toString());
            ObservableList<String> langs = FXCollections.observableArrayList(newList);

            comboBox.setItems(langs);
            comboBox.setValue(newList.get(0));
        }
    }

    private void InitComboBoxStep()
    {
        ObservableList<String> langs = FXCollections.observableArrayList(new ArrayList<String>(){
                 {
                     add("100 Гц");
                     add("10 Гц");
                 }
            }
        );
        comboBoxStep.setItems(langs);
        comboBoxStep.setValue(langs.get(0));
    }



    public void OnUpClicked(ActionEvent actionEvent) {
       try{
           String valueStepStr = String.valueOf(comboBoxStep.getValue());
           String[] valueStep = valueStepStr.split(" ");
           double step = Double.parseDouble(valueStep[0])/1000.0;


           String valueStr = freqTextField.getText();
           double value = Double.parseDouble(valueStr);

           double result = Math.round((value + step) * 1000.0)/1000.0;;

           if(result > 30000.0)
               result = 30000.0;
           if(result < 0.0)
               result = 1500.0;

           freqTextField.setText(result+"");
       }
       catch (Exception ex)
       {

       }
    }


    public void OnDownClicked(ActionEvent actionEvent) {
        try{
            String valueStepStr = String.valueOf(comboBoxStep.getValue());
            String[] valueStep = valueStepStr.split(" ");
            double step = Double.parseDouble(valueStep[0])/1000.0;

            String valueStr = freqTextField.getText();
            double value = Double.parseDouble(valueStr);

            double result = Math.round((value - step) * 1000.0)/1000.0;

            if(result < 1500.0)
                result = 1500.0;

            freqTextField.setText(result+"");
        }
        catch (Exception ex)
        {

        }
    }
}
