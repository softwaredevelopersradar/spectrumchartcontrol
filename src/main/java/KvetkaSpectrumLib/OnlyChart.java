package KvetkaSpectrumLib;


import KvetkaSpectrumLib.CustomIndicators.XRangeIndicatorCustom;
import KvetkaSpectrumLib.CustomIndicators.XValueIndicatorCustom;
import KvetkaSpectrumLib.CustomIndicators.XYValueIndicatorCustom;
import KvetkaSpectrumLib.CustomIndicators.YValueIndicatorCustom;
import KvetkaSpectrumLib.CustomStringConverter.CustomTickLabelFormatter;
import KvetkaSpectrumLib.CustomZoom.CustomZoomer;
import KvetkaSpectrumLib.Drawing.DrawArea;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.AxisMode;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.plugins.ChartPlugin;
import de.gsi.chart.plugins.XValueIndicator;
import de.gsi.chart.renderer.ErrorStyle;
import de.gsi.chart.renderer.datareduction.DefaultDataReducer;
import de.gsi.chart.renderer.spi.ErrorDataSetRenderer;
import de.gsi.chart.ui.geometry.Side;
import de.gsi.dataset.spi.DoubleDataSet;
import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.Arrays;

/**
 * Класс графика спектра
 * @autor Ягелло Александр, инженер-программист
 * @company ОАО "КБ Радар"
 * @version 1.0
 */
public class OnlyChart extends XYChart {
    private static int ARROWS_MAX_AMOUNT = 35;
    private int BUFFER_CAPACITY = 300000;
    private double maximumXValue = 30000;
    private double minimumXValue = 1500;
    private double maximumYValue = 30;
    private double minimumYValue = -120;
    private final double defaultXValueCrossIndicator = 10000;
    private final double defaultYValueCrossIndicator = -30;
    private final double thresholdIndicatorValue = -80;
    private int selectEvery_Count = 1;
    private boolean isDisplayFullSpectrum = true;

    private final String axisXName = "Частота";
    private final String axisXUnit = "кГц";
    private final String axisYName = "Уровень";
    private final String axisYUnit = "Дб";
    private final String crossIndicatorXName = "Частота";
    private final String crossIndicatorYName = "Уровень";
    private final String thresholdIndicatorName= "Порог";

    private DrawArea drawArea;

    private final CustomZoomer zoom = new CustomZoomer();
    private final ErrorDataSetRenderer eRenderer = new ErrorDataSetRenderer();
    private final DoubleDataSet dataBuffer = new DoubleDataSet("data", BUFFER_CAPACITY);

    private YValueIndicatorCustom yValueIndicatorCustom;
    private XYValueIndicatorCustom xyValueIndicatorCustom;
    private XValueIndicatorCustom yValueIndicator;



    public OnlyChart()
    {
        super(new DefaultNumericAxis(),new DefaultNumericAxis());
        initComponents();
    }



    public void setDisplayFullSpectrum(boolean displayFullSpectrum) { isDisplayFullSpectrum = displayFullSpectrum;}

    public void setSelectEvery_Count(int count) { this.selectEvery_Count = count; }

    public void setMaximumXValue(double maximumXValue) { this.maximumXValue = maximumXValue; }

    public void setMinimumXValue(double minimumXValue) { this.minimumXValue = minimumXValue; }

    public void setYValueIndicatorValue(double value) { this.yValueIndicatorCustom.setValue(value); }

    public void setZoomRange(double leftZoomBound, double rightZoomBound) {zoom.setZoomRange(leftZoomBound, rightZoomBound);}

    public void setZoomDepth(double zoomDepth) {zoom.setZoomDepth(zoomDepth);}

    public void setBufferCapacity(int bufferCapacity) { this.BUFFER_CAPACITY = bufferCapacity;}

    public void setXTranslation(int xTranslation)
    {
        this.getPlotArea().setTranslateX(xTranslation);
        this.getXAxis().getCanvas().setTranslateX(xTranslation);
        this.getYAxis().getCanvas().setTranslateX(xTranslation);
    }

    public void madeThresholdUnmovable() {
        yValueIndicatorCustom.setEditable(false);}

    public void setMaximumYValue(double maximumYValue){
            this.getYAxis().set(this.minimumYValue, maximumYValue);
            this.maximumYValue = maximumYValue;
            this.updateAxisRange();
        }

    public DrawArea getDrawArea() {return drawArea; }

    public double getMaximumXValue() { return this.maximumXValue; }

    public double getMinimumXValue() { return  this.minimumXValue; }

    public double getValue_Porog() { return yValueIndicatorCustom.getValue(); }

    public double getValueX_Cross() { return xyValueIndicatorCustom.getValueX(); }

    public double getValueY_Cross() { return xyValueIndicatorCustom.getValueY(); }

    public DoubleProperty getValueProperty_Porog() { return yValueIndicatorCustom.valueProperty(); }

    public DoubleProperty getValuePropertyY_Cross() { return xyValueIndicatorCustom.valuePropertyY(); }

    public DoubleProperty getValuePropertyX_Cross() { return xyValueIndicatorCustom.valuePropertyX(); }

    public double[] getXList() { return this.getDataSetForAxis(this.getXAxis()).get(0).getValues(0); }

    public double[] getYList() {
        synchronized(this) {
        return this.getDataSetForAxis(this.getYAxis()).get(0).getValues(1);
        }
    }

    /**
     * get point in plot coordinates (frequency, level)
     *
     * @param x x coordinate of mouse click in the canvas coordinate system
     * @param y y coordinate of mouse click in the canvas coordinate system
     * @return point in plot coordinates (frequency, level)
     */
    public Point2D getValuePosFromMouseClick(double x, double y)
    {
        Point2D c = this.getPlotArea().sceneToLocal(x, y);
        final double yPosData = this.getYAxis().getValueForDisplay(c.getY());
        final double xPosData = this.getXAxis().getValueForDisplay(c.getX());
        return new Point2D(xPosData,yPosData);
    }

    /**
     * displays a new dataset
     *
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(byte[] dataSpectr) {
        MassOfPoints massOfPoints = null;

        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(minimumXValue, maximumXValue, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(minimumXValue, maximumXValue, dataSpectr);


       if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }

    /**
     *  displays a new dataset
     *
     * @param endFreq end frequency of the displayed spectrum
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(double endFreq, byte[] dataSpectr) {
        MassOfPoints massOfPoints = null;
        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(minimumXValue,endFreq, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(minimumXValue,endFreq, dataSpectr);

        if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }

    /**
     * displays a new dataset
     *
     * @param startFreq start frequency of the displayed spectrum
     * @param endFreq end frequency of the displayed spectrum
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(double startFreq, double endFreq, byte[] dataSpectr) {
        MassOfPoints massOfPoints = null;
        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(startFreq,endFreq, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(startFreq,endFreq, dataSpectr);

        if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }

    /**
     * displays a new dataset
     *
     * @param startFreq array of start frequency of the displayed spectrum parts
     * @param endFreq array of end frequency of the displayed spectrum parts
     * @param dataSpectr arrays of level values
     */
    public void PlotSpectr(double[] startFreq, double[] endFreq, byte[][] dataSpectr) {
        for(int k = 0; k < startFreq.length; k++)
        {
            MassOfPoints massOfPoints = createSpectrData(startFreq[k], endFreq[k], dataSpectr[k]);
            if(massOfPoints == null)
                continue;
            massOfPoints.Y = addElementInFrontOfArray(massOfPoints.Y, -300);
            massOfPoints.X = addElementInFrontOfArray(massOfPoints.X,startFreq[k] - 0.1);
            massOfPoints.Y = addElementInEndOfArray(massOfPoints.Y, -300);
            massOfPoints.X = addElementInEndOfArray(massOfPoints.X,endFreq[k] - 0.1);

            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
        }
    }


    /**
     * displays a new sub dataset on the right part of control
     *
     * @param startFreq start frequency of the displayed spectrum
     * @param endFreq end frequency of the displayed spectrum
     * @param dataSpectr array of level values
     */
    public double[] PlotSubSpectr(double startFreq, double endFreq, byte[] dataSpectr) {
        dataBuffer.clearData();
        return createSubSpectrData(startFreq,endFreq, dataSpectr);
    }

    /**
     * clear a part of dataset
     *
     * @param startFreq start frequency of delete part
     * @param endFreq end frequency of delete part
     */
    public void ClearSpectrPart(double startFreq, double endFreq)
    {
        int indStart = this.getAllDatasets().get(0).getIndex(0,startFreq);
        if(this.getAllDatasets().get(0).getValue(0,indStart) < startFreq)
            indStart++;

        int indEnd = this.getAllDatasets().get(0).getIndex(0,endFreq);
        if(this.getAllDatasets().get(0).getValue(0,indEnd) > endFreq)
            indEnd++;

        dataBuffer.set(indStart, startFreq, -300);
        dataBuffer.set(indEnd - 1, endFreq, -300);
        dataBuffer.remove(indStart + 1, indEnd - 1);
    }

    /**
     * clear dataset
     */
    public void ClearSpectr()
    {
        dataBuffer.clearData();
    }

    /**
     *  added the indicator of the selected signal
     *
     * @param startRange frequency of the left edge of the range indicator
     * @param endRange frequency of the right edge of the range indicator
     */
    public void addXRangeIndicator(double startRange, double endRange)
    {
        XRangeIndicatorCustom xRangeIndicatorCustom;
        xRangeIndicatorCustom = new XRangeIndicatorCustom(this.getXAxis(), startRange, endRange);
        xRangeIndicatorCustom.setEditable(true);
        changeColorAllXRangeIndicator();
        xRangeIndicatorCustom.setColor(Color.RED);
        this.getPlugins().add(xRangeIndicatorCustom);
    }

    public void updateXRangeIndicator(double newStartRange, double newEndRange, double oldFrequency) {
        XRangeIndicatorCustom xRangeIndicatorCustom = findXRangeIndicator(oldFrequency);
        if (xRangeIndicatorCustom != null)
        {
            xRangeIndicatorCustom.setUpperBound(newEndRange);
            xRangeIndicatorCustom.setLowerBound(newStartRange);
            xRangeIndicatorCustom.layoutChildren();
        }
    }

    public XRangeIndicatorCustom findXRangeIndicator(double freq)
    {
        for (ChartPlugin plugin : this.getPlugins()){
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                if(((XRangeIndicatorCustom) plugin).getLowerBound() <= freq && ((XRangeIndicatorCustom) plugin).getUpperBound() >= freq)
                    return (XRangeIndicatorCustom) plugin;
            }
        }
        return null;
    }

    /**
     *  change color of All signal indicators. Identifier frequency
     */
    public void changeColorXRangeIndicator(double freq)
    {
        for (ChartPlugin plugin : this.getPlugins()){
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                if(((XRangeIndicatorCustom) plugin).getLowerBound() <= freq && ((XRangeIndicatorCustom) plugin).getUpperBound() >= freq)
                    ((XRangeIndicatorCustom) plugin).setColor(Color.RED);
                else ((XRangeIndicatorCustom) plugin).setColor(Color.BLUE);
            }
        }
        this.updatePluginsArea();
    }

    /**
     *  deletes the indicator of the selected signal
     */
    public void deleteRangeIndicator(double freq)
    {
        XRangeIndicatorCustom xRangeIndicatorCustom = findXRangeIndicator(freq);
        if(xRangeIndicatorCustom != null)
            this.getPlugins().remove(xRangeIndicatorCustom);
    }

    /**
     * made frequency and level indicator lines unvisible. Need for sub spectr chart
     */
    public void madeCrossLineControlUnVisible()
    {
        this.getPlugins().remove(xyValueIndicatorCustom);
        xyValueIndicatorCustom = null;
    }

    /**
     * change min pixel distance for reduction algorithm
     *
     * @param minPixelDistance The larger the value, the faster the output works, the smaller the more correct the values will be displayed
     */
    public void setMinPixelDistanceForReductionAlgorithm(int minPixelDistance)// чем больше значение, тем быстрее отображение, но могут теряться точки
    {
        ((DefaultDataReducer) eRenderer.getRendererDataReducer()).setMinPointPixelDistance(minPixelDistance);
    }

    public void disabledZoomer()
    {
        this.getPlugins().remove(zoom);
    }

    private void initComponents() {
        this.setAnimated(false);
        this.setLegendVisible(false);
        this.getAxesAndCanvasPane().getRowConstraints().get(2).setPrefHeight(50);

        InitAxis();
        InitZoomer();
        InitErrorDataSetRenderer();
        InitIndicators();

        drawArea = new DrawArea(this);

        dataBuffer.setStyle("strokeColor=" + Color.rgb(255,201,0)+";" +"strokeWidth=1.5;");


        this.getXAxis().invertAxis(false);

    }


    private void InitErrorDataSetRenderer() {
        eRenderer.setErrorType(ErrorStyle.NONE);
        eRenderer.setDrawMarker(false);
        eRenderer.getAxes().addAll(this.getYAxis());
        eRenderer.getDatasets().add(dataBuffer);
        this.getRenderers().add(eRenderer);
    }

    private void InitZoomer()
    {
        zoom.setSliderVisible(false);
        zoom.setAddButtonsToToolBar(false);
        zoom.setAxisMode(AxisMode.X);
        zoom.setZoomInMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED && mouseEvent.getClickCount() == 1);
        zoom.setZoomOriginMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getClickCount() == 2);
        zoom.setZoomOutMouseFilter(mouseEvent -> false);
        this.getPlugins().add(zoom);

    }

    private void InitIndicators()
    {
        yValueIndicatorCustom = new YValueIndicatorCustom(this.getYAxis(), thresholdIndicatorValue, thresholdIndicatorName);
        yValueIndicatorCustom.setColor(Color.RED);
        xyValueIndicatorCustom = new XYValueIndicatorCustom(this.getXAxis(), this.getYAxis(), defaultXValueCrossIndicator, defaultYValueCrossIndicator, this.getPlotArea(),crossIndicatorXName, crossIndicatorYName);

        this.getPlugins().add(yValueIndicatorCustom);
        this.getPlugins().add(xyValueIndicatorCustom);
    }

    private void InitAxis()
    {
        this.getXAxis().setName(axisXName); // init X axis
        this.getXAxis().setUnit(axisXUnit);
        this.getXAxis().set(minimumXValue, maximumXValue);
        this.getXAxis().setAutoRanging(false);
        this.getXAxis().getCanvas().setVisible(true);
        this.getXAxis().getCanvas().setWidth(20);
        this.getPlotArea().setBorder(new Border(new BorderStroke(Color.WHITE,
                BorderStrokeStyle.SOLID,
                new CornerRadii(0d), new BorderWidths(0,0,1,0))));
        this.getAxesAndCanvasPane().setPadding(new Insets(0,0,-15,0));

        ((DefaultNumericAxis)this.getXAxis()).setTickLabelFormatter(new CustomTickLabelFormatter());

        this.getYAxis().setName(axisYName); // init Y axis
        this.getYAxis().setUnit(axisYUnit);
        this.getYAxis().setAutoRanging(false);
        this.getYAxis().setSide(Side.LEFT);
        this.getYAxis().set(minimumYValue, maximumYValue);
    }


    private int[] addElementInFrontOfArray(int[] elements, int element)
    {
        int[] newArray = Arrays.copyOf(elements, elements.length + 1);
        newArray[0] = element;
        System.arraycopy(elements, 0, newArray, 1, elements.length);

        return newArray;
    }

    private int[] addElementInEndOfArray(int[] arr, int element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }

    private double[] addElementInFrontOfArray(double[] elements, double element)
    {
        double[] newArray = Arrays.copyOf(elements, elements.length + 1);
        newArray[0] = element;
        System.arraycopy(elements, 0, newArray, 1, elements.length);

        return newArray;
    }

    private double[] addElementInEndOfArray(double[] arr, double element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }


    private MassOfPoints createSpectrData(double startFreq, double endFreq, byte[] dataSpectr)
    {
        double y = (endFreq-startFreq) / dataSpectr.length;
        int length = dataSpectr.length / selectEvery_Count;
        double[] xx = new double[length];
        double[] yy = new double[length];
        int ind = 0;

        try{
            ind = this.getAllDatasets().get(0).getIndex(0,startFreq);

            if(this.getAllDatasets().get(0).getValue(0,ind) < startFreq && this.getAllDatasets().get(0).getDataCount() > 5)
                ind++;

            for (int i = 0, j = 0; i < dataSpectr.length; i += selectEvery_Count, j++) {
                xx[j] = y * i + startFreq;
                yy[j] = ((-1)*(dataSpectr[i]));
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return new MassOfPoints(ind, xx, yy);
    }



    private MassOfPoints createFullSpectrData(double startFreq, double endFreq, byte[] dataSpectr)
    {
        double y = (endFreq-startFreq) / dataSpectr.length;

        int length = dataSpectr.length / selectEvery_Count;

        double[] xx = new double[length];
        double[] yy = new double[length];

        for (int i = 0, j = 0; i < dataSpectr.length; i += selectEvery_Count, j++) {
            xx[j] = y * i + startFreq;
            yy[j] = (-1) * dataSpectr[i];
        }

        return new MassOfPoints(0, xx, yy);
    }



    private double[] createSubSpectrData(double startFreq, double endFreq, byte[] dataSpectr)
    {
        double y = (endFreq-startFreq) / dataSpectr.length;

        int length = dataSpectr.length;

        double[] xx = new double[length];
        double[] yy = new double[length];

        for (int i = 0; i < dataSpectr.length; i++) {
             xx[i] = y * i + startFreq;
             yy[i] = (-1) * dataSpectr[i];
        }

        dataBuffer.add(xx,yy);
        return yy;
    }


    private void changeColorAllXRangeIndicator()
    {
        for (ChartPlugin plugin : this.getPlugins()) {
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                ((XRangeIndicatorCustom) plugin).setColor(Color.BLUE);
            }
        }
    }
}



class MassOfPoints
{
    public MassOfPoints(int size)
    {
        X = new double[size];
        Y = new double[size];
    }

    public MassOfPoints(int startIndex, double[] X, double[] Y)
    {
        this.startIndex = startIndex;
        this.X = X;
        this.Y = Y;
    }

    int startIndex = 0;
    double[] X;
    double[] Y;
}