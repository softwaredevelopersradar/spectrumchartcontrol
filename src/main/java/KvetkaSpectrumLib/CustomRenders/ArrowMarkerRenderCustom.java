package KvetkaSpectrumLib.CustomRenders;

import de.gsi.chart.XYChart;
import de.gsi.chart.XYChartCss;
import de.gsi.chart.axes.Axis;
import de.gsi.chart.renderer.spi.LabelledMarkerRenderer;
import de.gsi.chart.utils.StyleParser;
import de.gsi.dataset.DataSet;
import javafx.geometry.Orientation;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;

import java.util.Objects;

public class ArrowMarkerRenderCustom extends LabelledMarkerRenderer {

    private boolean isShowDegrees = false;
    public void IsShowDegrees(boolean isShowDegrees)
    {
        this.isShowDegrees = isShowDegrees;
    }


    @Override
    protected void drawVerticalLabelledMarker(final GraphicsContext gc, final XYChart chart, final DataSet dataSet,
                                              final int indexMin, final int indexMax) {
        Axis xAxis = this.getFirstAxis(Orientation.HORIZONTAL);
        if (xAxis == null) {
            xAxis = chart.getFirstAxis(Orientation.HORIZONTAL);
        }

        Axis yAxis = this.getFirstAxis(Orientation.VERTICAL);
        if (yAxis == null) {
            yAxis = chart.getFirstAxis(Orientation.VERTICAL);
        }
        setGraphicsContextAttributes(gc,dataSet.getStyle());

        final double height = chart.getCanvas().getHeight();
        for (int i = 0; i < indexMax; i++) {

            final Color strokeColor = StyleParser.getColorPropertyValue(dataSet.getStyle(i), XYChartCss.STROKE_COLOR);
            final double screenX = xAxis.getDisplayPosition(dataSet.get(DataSet.DIM_X, i));
            final double screenY = yAxis.getDisplayPosition(dataSet.get(DataSet.DIM_Y, i));
            final double upY = screenY - 50;
            final double downY = screenY - 12;

            if (strokeColor == null) {
                gc.setStroke(Color.RED);
            } else {
                gc.setStroke(strokeColor);
            }
            gc.strokeLine(screenX, downY, screenX, upY);
            gc.strokeLine(screenX, downY, screenX - 5, downY - 14);
            gc.strokeLine(screenX, downY, screenX + 5, downY - 14);
            gc.restore();
        }
    }


@Override
    protected void setGraphicsContextAttributes(final GraphicsContext gc, final String style) {
        gc.setStroke(Color.RED);

        final Color fillColor = StyleParser.getColorPropertyValue(style, XYChartCss.FILL_COLOR);
        if (fillColor == null) {
            gc.setFill(Color.valueOf("#6c92fe"));
        } else {
            gc.setFill(fillColor);
        }

        final Double strokeWidth = StyleParser.getFloatingDecimalPropertyValue(style, XYChartCss.STROKE_WIDTH);
        gc.setLineWidth(Objects.requireNonNullElseGet(strokeWidth, () -> strokeLineWidthMarker));

        final Font font = StyleParser.getFontPropertyValue(style);
            gc.setFont(font);

        gc.save();
    }

}
