package KvetkaSpectrumLib.CustomRenders;

public class Point {

    public Point (int ind, double val)
    {
        this.Ind = ind;
        this.Val = val;
    }

    public int Ind;
    public double Val;
}
