package KvetkaSpectrumLib.CustomRenders;

import de.gsi.chart.renderer.datareduction.ReductionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class CustomDataReducer3D {

    public static void resample(double[] src, final int srcWidth, final int srcHeight, double[] target,
                                final int targetWidth, final int targetHeight, //
                                ReductionType reductionType) {

        final long xRatio = (((long)srcWidth << 16) / targetWidth) + 1;
        final long yRatio = (((long)srcHeight << 16) / targetHeight) + 1;
        final long xLimit = (int)(xRatio >> 16);
        final long yLimit = (int)(yRatio >> 16);

        final double norm = (xLimit * yLimit);
        switch (reductionType) {
            case MIN:
                for (int i = 0; i < targetHeight; i++) {
                    final int y2 =(int) ((i * yRatio) >> 16);
                    final int srcRowStart = y2 * srcWidth;
                    final int targetRowStart = i * targetWidth;
                    for (int j = 0; j < targetWidth; j++) {
                        final int x2 = (int)((j * xRatio) >> 16);
                        double val = Double.MAX_VALUE;
                        for (int k = 0; k < yLimit; k++) {
                            final int srcRowOffset = k * srcWidth;
                            for (int l = 0; l < xLimit; l++) {
                                val = Math.min(val, src[srcRowStart + srcRowOffset + x2 + l]);
                            }
                        }
                        target[targetRowStart + j] = val;
                    }
                }
                break;
            case MAX:

//               List<Point> points = new ArrayList<Point>();
//               int indexBeginSignal = 0;
//               int indexEndSignal = 0;
//
//                for (int i = 0; i < srcWidth; i++ ) {
//                    if(src[i] > -80)
//                    {
//                        indexBeginSignal = i;
//                        while(src[i] > -80)
//                        {
//                            i++;
//                        }
//                        indexEndSignal = i;
//                        int ind = (indexBeginSignal + indexEndSignal)/2;
//                        points.add(new Point(ind,src[ind]));
//                    }
//                }
//
//                int pointsBeetwen = 0;
//                int currentIndexTarget = 0;
//                double val1 = -Double.MAX_VALUE;
//
//                for(Point point : points)
//                {
//                    pointsBeetwen = Math.abs(pointsBeetwen - point.Ind);
//
//                    int amountOfAddedPoints = pointsBeetwen/(srcWidth/target.length);
//
//
//                    for (int i = 0; i < amountOfAddedPoints; i++ )
//                    {
//
//                        target[currentIndexTarget] = Math.max(val1, src[(currentIndexTarget)*(srcWidth/target.length)]);
//                        currentIndexTarget++;
//                    }
//
//
//                    target[currentIndexTarget+1]=point.Val;
//
//                    pointsBeetwen = point.Ind;
//
//
//                }

                for (int i = 0; i < targetHeight; i++) {
                    final int y2 =(int) ((i * yRatio) >> 16);
                    final int srcRowStart = y2 * srcWidth;
                    final int targetRowStart = i * targetWidth;
                    for (int j = 0; j < targetWidth; j++) {
                        final int x2 =(int) ((j * xRatio) >> 16);
                        double val = -Double.MAX_VALUE;
                        for (int k = 0; k < yLimit; k++) {
                            final int srcRowOffset = k * srcWidth;
                            for (int l = 0; l < xLimit; l++) {
                                 val = Math.max(val, src[srcRowStart + srcRowOffset + x2 + l]);
                            }

                        }
                        target[targetRowStart + j] = val;
                    }
                }
                break;

            case DOWN_SAMPLE:
                for (int i = 0; i < targetHeight; i++) {
                    final int y2 =(int) ((i * yRatio) >> 16);
                    final int srcRowStart = y2 * srcWidth;
                    final int targetRowStart = i * targetWidth;
                    for (int j = 0; j < targetWidth; j++) {
                        final int x2 = (int)((j * xRatio) >> 16);
                        target[targetRowStart + j] = src[srcRowStart + x2];
                    }
                }
                break;
            case AVERAGE:
            default:
                for (int i = 0; i < targetHeight; i++) {
                    final int y2 =(int) ((i * yRatio) >> 16);
                    final int srcRowStart = y2 * srcWidth;
                    final int targetRowStart = i * targetWidth;
                    for (int j = 0; j < targetWidth; j++) {
                        final int x2 =(int)((j * xRatio) >> 16);
                        double val = 0.0;
                        for (int k = 0; k < yLimit; k++) {
                            final int srcRowOffset = k * srcWidth;
                            for (int l = 0; l < xLimit; l++) {
                                val += src[srcRowStart + srcRowOffset + x2 + l];
                            }
                        }
                        target[targetRowStart + j] = val / norm;
                    }
                }
                break;
        }
    }

    }