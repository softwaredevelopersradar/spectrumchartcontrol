package KvetkaSpectrumLib.Interfaces;

import KvetkaModels.AnalogReconFWSModel;

public interface ICallBackSound {

    public void Playing(AnalogReconFWSModel analogReconFWSMode, boolean isPlaying);
}
