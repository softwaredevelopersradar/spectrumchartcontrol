package KvetkaSpectrumLib.Interfaces;

public interface ICallBackFilters {

    /**
     * occurs if one of four buttons on Athenuators control clicked
     *
     * @param buttonText text on buttons of Athenuators control
     */
    public void buttonFilterSelected(double buttonText);

}
