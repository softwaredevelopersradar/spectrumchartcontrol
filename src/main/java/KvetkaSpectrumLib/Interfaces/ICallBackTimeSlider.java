package KvetkaSpectrumLib.Interfaces;


public interface ICallBackTimeSlider {
    void SpectrumRequest(int timeInSeconds);
}
