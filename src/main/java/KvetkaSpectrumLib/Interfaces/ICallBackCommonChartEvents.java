package KvetkaSpectrumLib.Interfaces;

public interface ICallBackCommonChartEvents {
    /**
     * occurs if threshold Changed
     *
     * @param threshold changed threshold
     */
    public void thresholdChanged(double threshold);
}
