package KvetkaSpectrumLib.Interfaces;

import KvetkaModels.AnalogReconFWSModel;

public interface ICallBackFrequencyChangedInSubSpectrum {
     void FrequencyChanged(double oldValue, double newValue, int idOfSelectedSignalButton);
}
