package KvetkaSpectrumLib.Interfaces;

public interface ICallBackAthenuators {

    /**
     * occurs if one of four buttons on Athenuators control clicked
     *
     * @param buttonText text on buttons of Athenuators control
     */
    public void buttonAttenuatorSelected(int buttonText);

}
