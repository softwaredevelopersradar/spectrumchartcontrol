package KvetkaSpectrumLib.Signal;

import KvetkaModels.AnalogReconFWSModel;
import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

import java.io.IOException;

public class SignalButton extends Pane {

    @FXML
    private Label avFreqLabel;
    @FXML
    private CheckBox checkBox;
    @FXML
    private Circle existSignalMarker;

    private int id;

    private final RadialGradient gradientPaint_Green = new RadialGradient(360,30,0,0,10,false, CycleMethod.NO_CYCLE,
            new Stop(0, Color.rgb(160,255,130)), new Stop(1, Color.rgb(30,98,10)));
    private final RadialGradient gradientPaint_Red = new RadialGradient(360,20,0,0,10,false, CycleMethod.NO_CYCLE,
            new Stop(0, Color.rgb(255,160,160)), new Stop(1, Color.rgb(144,2,2)));
    private SignalParameters signalParameters;



    public SignalButton() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/SignalIndicator.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }



    public void setFreq(double freq) {
        avFreqLabel.setText(String.valueOf(Math.round(freq * 10.0)/10.0).concat(" кГц"));
    }

    public void setSignalParameters(SignalParameters signalParameters) {
        this.signalParameters = signalParameters;
        setIndicatorOfExistSignal(signalParameters.isSignalExist());
        setFreq(signalParameters.getFrequency());
    }

    public void setIndicatorOfExistSignal(boolean isSignalExist)
    {
        if(isSignalExist)
            existSignalMarker.setFill(gradientPaint_Green);
        else
            existSignalMarker.setFill(gradientPaint_Red);
        signalParameters.setSignalExist(isSignalExist);
    }

    public AnalogReconFWSModel getAnalogReconFWSModel()
    {
        AnalogReconFWSModel analogReconFWSModel = signalParameters.getAnalogReconFWSModel();
        analogReconFWSModel.setId(id);
        return analogReconFWSModel;
    }

    public boolean getIsCheckedStatus() { return checkBox.isSelected();}

    public SignalParameters getSignalParameters() { return signalParameters; }

    public void setSignalId(int num)
    {
        id = num;
        checkBox.setText(String.valueOf(num));
    }

    public int getSignalId()
    {
        return id;
    }
}
