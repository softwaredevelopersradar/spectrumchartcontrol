package KvetkaSpectrumLib.Signal;

public class SignalMath {


    public static byte[] ConvertDoubleMassiveToByte(double[] dataset)
    { if(dataset == null || dataset.length == 0) return null;
        byte[] newIntDataSet = new byte[dataset.length];
        for(int i = 0; i < newIntDataSet.length; i++)
        {
            try
            {
            newIntDataSet[i] = (byte) dataset[i];
            }catch (Exception ex)
            {
                newIntDataSet[i] = 120;
            }
        }
        return  newIntDataSet;
    }

    private static void absOfMassive(int[] newDataSet)
    {
        if(newDataSet == null || newDataSet.length == 0) return;
        if(newDataSet[10] <=0)
        {
            for(int i = 0; i < newDataSet.length; i++)
                newDataSet[i] = Math.abs(newDataSet[i]);
        }
    }

    public static boolean isSignalExist(double[] dataset,double porog)
    {   if(dataset == null || dataset.length == 0) return false;
        return isSignalExist(0,dataset.length,dataset, porog);
    }


    public static boolean isSignalExist(int indexAtWhichSelectedAreaStarts, int indexAtWhichSelectedAreaEnds, double[] dataset, double porog)
    {
        if(dataset == null || dataset.length == 0 || (indexAtWhichSelectedAreaEnds == 0 && indexAtWhichSelectedAreaStarts == 0)) return false;


        boolean deleteFlag = false;
        for(int i = indexAtWhichSelectedAreaStarts + 1; i < indexAtWhichSelectedAreaEnds; i++)
        {
            if(dataset[i - 1] < porog && dataset[i] >= porog)
            {
                while (i < indexAtWhichSelectedAreaEnds - 1)
                {
                    if(dataset[i] >= porog)
                    {
                        deleteFlag = true;
                        break;
                    }
                    i++;
                }
                break;
            }
        }

        return deleteFlag;
    }


    public static double CalculateFrequency(double[] newDataSet, double startFrequency, double endFrequency, double porog)
    {
        if(newDataSet == null || newDataSet.length == 0) return (startFrequency + endFrequency)/2.0;
        double maxLevelIndex = FindMaxLevelIndex(newDataSet, porog);
        return Math.round((startFrequency + maxLevelIndex * (Math.abs(endFrequency - startFrequency)/newDataSet.length))*100000.)/100000.;
    }

    public static double CalculateFrequency(double[] newDataSet, double startFrequency, double endFrequency)
    {
        if(newDataSet == null || newDataSet.length == 0) return (startFrequency + endFrequency)/2.0;
        double maxLevelIndex = FindMaxLevelIndex(newDataSet,256);
        return Math.round((startFrequency + maxLevelIndex * (Math.abs(endFrequency - startFrequency)/newDataSet.length))*100000.)/100000.;
    }

    public static double CalculateMiddleFrequency(double startFrequency, double endFrequency)
    {
        return (startFrequency + endFrequency)/2.0;
    }

    public static double FindMaxLevel(double[] newDataSet)
    {
        if(newDataSet == null || newDataSet.length == 0) return 0.0;

        double maxLevelValue = newDataSet[0];
        for (double b : newDataSet) {
            if (b > maxLevelValue) {
                maxLevelValue = b;
            }
        }
        return maxLevelValue;
    }

    public static double FindMaxLevelIndex(double[] newDataSet, double porog)
    {
        if(newDataSet == null || newDataSet.length == 0 ) return 0.0;
        double maxLevelValue = newDataSet[0];
        int  indexFirstAppearMaxVerticalValue = 0;
        int  indexSecondAppearMaxVerticalValue = 0;
        for(int i = 0; i < newDataSet.length; i++)
        {
            if (newDataSet[i] > maxLevelValue) {
                maxLevelValue = newDataSet[i];
                indexFirstAppearMaxVerticalValue = i;
            }
        }

        indexSecondAppearMaxVerticalValue = indexFirstAppearMaxVerticalValue;
        for(int i = indexFirstAppearMaxVerticalValue; i < newDataSet.length; i++)
        {
            if(newDataSet[i] < maxLevelValue)
            {
                indexSecondAppearMaxVerticalValue = i - 1;
                break;
            }
        }

        if(maxLevelValue >= Math.abs(porog))
            return newDataSet.length / 2;

        return indexFirstAppearMaxVerticalValue + (Math.abs(indexSecondAppearMaxVerticalValue - indexFirstAppearMaxVerticalValue) / 2.);
    }

    public static double CalculateDeltaFForSignal(double[] newDataSet, double startFrequency, double endFrequency)
    {
        if(newDataSet == null || newDataSet.length == 0) return 0.0;
        int leftIndexOfDeltaF = 0;
        int rightIndexOfDeltaF = 0;
        final double maxLevelValue = FindMaxLevel(newDataSet);
        final int indexOfMaxVerticalValue = (int) FindMaxLevelIndex(newDataSet, 256);

        for(int i = indexOfMaxVerticalValue; i > 0; i--)
        {
            if(newDataSet[i] > (maxLevelValue - 3))
            {
                leftIndexOfDeltaF = i+1;
                break;
            }
        }

        for(int i = indexOfMaxVerticalValue; i < newDataSet.length; i++)
        {
            if(newDataSet[i] > (maxLevelValue - 3))
            {
                rightIndexOfDeltaF = i-1;
                break;
            }
        }

        double deltaFValue = 0.0;
        final double deltaFrequency = Math.abs(endFrequency - startFrequency) / newDataSet.length;
        if(newDataSet[leftIndexOfDeltaF] != maxLevelValue + 3)
        {
            if(leftIndexOfDeltaF > 0){
                double P1x = leftIndexOfDeltaF;
                double P1y = newDataSet[leftIndexOfDeltaF];
                double P2x = leftIndexOfDeltaF - 1;
                double P2y = newDataSet[leftIndexOfDeltaF - 1];

                final double deltaFValueLeftPartIndex = (((P2x-P1x) * ((maxLevelValue + 3) - P1y))/(P2y - P1y) + P1x);

                P1x = rightIndexOfDeltaF;
                P2x = rightIndexOfDeltaF + 1;
                P1y = newDataSet[rightIndexOfDeltaF];
                P2y = newDataSet[rightIndexOfDeltaF+1];

                final double deltaFValueRightPartIndex = (((P2x-P1x) * ((maxLevelValue + 3) - P1y))/(P2y - P1y) + P1x);
                deltaFValue = (deltaFValueRightPartIndex - deltaFValueLeftPartIndex) * deltaFrequency;
            }
        }
        return deltaFValue;
    }
}
